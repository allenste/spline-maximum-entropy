## Makefile
#  Steve Allen, CCS, Universite de Sherbrooke
#  September 2011
#
#  This Makefile compiles the C++ source code to produce a library
#  callable from Python.
##
# Copyright (C) 2011 Steve Allen
#
# This file is part of SplineMaxEnt.
# 
# SplineMaxEnt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SplineMaxEnt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SplineMaxEnt.  If not, see <http://www.gnu.org/licenses/>.

# Set if you want to use Blas or not and if you want to use OpenMp
use_blas := 1
use_omp := 0
# Choose the compiler among gcc (0), pgi (1), intel (2), pathscale (3), clang (4)
comp := 4
verbose := 1
debug := 0

DIR = -Isrc -DVERBOSE=$(verbose) -DUSE_LAPACK
LIBS =
ifeq (1,$(use_blas))
    DIR += -DUSE_BLAS
endif

ifeq (4,$(comp))
    LIBS += -llapack -lblas -lifcore -liomp5 -limf -lirc -lpthread
    CPP = clang++
    OMPFLAGS =
    OPT = -O2
else ifeq (3,$(comp))
    LIBS += -L/opt/lapack64/3.2/lib -L$(HOME)/lib -llapack -lblas -lgfortran
    CPP = pathCC
    OMPFLAG = -mp
    OPT = -O2
else ifeq (2,$(comp))
    # Mkl version 10 ou 11
    #LIBS += -lmkl_lapack -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lguide -lpthread
    # Mkl version 12
    LIBS += -lmkl_lapack95_lp64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread
    CPP = icpc
    OMPFLAG = -openmp
    OPT = -O2 -funroll-loops
else ifeq (1,$(comp))
    LIBS += -llapack -lblas -lpgftnrtl -lrt -lpthread
    CPP = pgCC
    OMPFLAG = -mp
    OPT = -O2 -Munroll=c:4
else
    LIBS += -llapack -lblas -lgfortran -lpthread
    CPP = g++
    OMPFLAG = -fopenmp
    OPT = -O2 -funroll-loops
endif

LFLAGS =
CPPFLAGS = -fPIC
ifeq (1,$(debug))
    CPPFLAGS += -g
    LFLAGS += -g
else
    CPPFLAGS += $(OPT)
endif
ifeq (1,$(use_omp))
    CPPFLAGS += $(OMPFLAG)
    LFLAGS += $(OMPFLAG)
    ifeq (0,$(use_blas))
        LIBS += -lpthread
    endif
endif
LINK = $(CPP)
LIBS += -lm

SRC = $(wildcard src/*.cpp)
OBJS = $(SRC:.cpp=.o)
TARGET = libmaxent
TARGETSO = $(TARGET).so
TARGETA = $(TARGET).a

.SUFFIXES: .cpp

maxent: $(OBJS) main.o
	$(LINK) $(LFLAGS) main.o $(OBJS) $(LIBS) -o $@

$(TARGETSO): $(OBJS)
	$(LINK) $(LFLAGS) -shared -Wl,-soname,$@ $(OBJS) $(LIBS) -o $@

$(TARGETA): $(OBJS)
	ar rcs $(TARGETA) $(OBJS)

.cpp.o:
	$(CPP) $(DIR) $(CPPFLAGS) -c $< -o $@

clean:
	rm -rf src/*~ src/*.o *.o

distclean:
	rm -rf src/*~ src/*.o *.o $(TARGET).* maxent

