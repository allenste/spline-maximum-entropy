"""
    @package SplineMaxEnt
    @file read_write.py
    @author Steve Allen, CCS, Universite de Sherbrooke
    @date September 2011

    @brief This file contains a driver that reads the data and call the C++
    library to perform the analytical continuation of the data by maximum
    entropy.

    @note Python Modules required: Numpy
"""
#
# Copyright (C) 2011 Steve Allen
#
# This file is part of SplineMaxEnt.
# 
# SplineMaxEnt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SplineMaxEnt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SplineMaxEnt.  If not, see <http://www.gnu.org/licenses/>.
#

import numpy as np

def getxmlValues(desc, *args) :
    """
        @brief This function build a dictionary using the list of nodes of an
        xml dom
    """
    children = desc[0].childNodes
    d = {}
    listargs = []
    for a in args :
        typename = type(a).__name__
        if (typename == 'list' or typename == 'tuple') :
            for i in a : listargs.append(i)
        else : listargs.append(a)
    for c in children :
        if hasattr(c, 'tagName') :
            s = c.tagName.lower()
            v = c.childNodes[0].data.replace(' ', '')
            for a in listargs :
                if (type(a).__name__ == list) : a = a[0]
                if s == a :
                    if a in d :
                        if type(d[a]).__name__ == 'list' :
                            d[a].append(v)
                        else : d[a] = [d[a], v]
                    else : d[a] = v
    return d

def readCfgFile (cfgFile, dico) :
    answer = []
    if cfgFile.find(r'.xml') >= 0 :
        import xml.dom.minidom as xml
        try :
            dom = xml.parse(cfgFile)
        except :
            print "Warning unable to parse xml file ", cfgFile
            print "Will use default parameter values"
            return answer
        for k, v in dico.iteritems() :
            desc = dom.getElementsByTagName(k)
            typename = type(v).__name__
            if typename == 'dict' :
                ks = v.keys()
                d = getxmlValues(desc, ks)
                if len(ks) > 1 :
                    l = []
                    for n in ks :
                        l.append(d.get(n, None))
                    answer.append(l)
                elif len(ks) == 1 : answer.append(d.get(ks[0], None))
                else : answer.append(None)
            elif typename == 'str' :
                if v == 'NodeList' : answer.append(desc)
                else :
                    d = getxmlValues(desc, v)
                    answer.append(d.get(v, None))
    else :
        print "Warning unable to open file ", cfgFile
        print "Will use default parameter values"
    return answer

def readData (datafile, *args) :
    kn = None
    G = None
    sigma = None
    # Interpret the additional arguments
    arglist = ['file', 'error', 'temp']
    d = {}
    for a in args :
        typename = type(a).__name__
        if typename == 'NodeList' :
            d = dict(d.items() + getxmlValues(a, arglist).items())
        elif typename == 'dict' :
            d = dict(d.items() + a.items())
    dfile = d.get('file', 'data.dat')
    error = float(d.get('error', 1.e-7))
    temp = d.get('temp', None)
    if temp != None : temp = float(temp)
    if datafile == None : datafile = dfile

    # Read the data
    try :
        d = np.loadtxt (datafile, dtype=np.float64)
        if len(d.shape) < 2 :
            print "file", datafile, "contains only one column."
            return (kn, G, sigma)
        ncol = d.shape[1]
        kn = 2 * np.pi * d[:,0]
        nkn = kn.size
        if ncol > 2 :
            G = d[:,1] + 1j*d[:,2]
        elif ncol == 2 :
            G = d[:,1]
        if ncol > 4 :
            sigma = d[:,3] + 1j*d[:,4]
        elif ncol == 4 :
            if G.dtype.name.find("complex") >= 0 :
                sigma = d[:,3] + 1j*d[:3]
            else :
                sigma = d[:,3]
    except :
        print "Unable to open data file", datafile

    # Complete the data
    if temp != None :
        kn = np.linspace(0, 2 * (G.size-1), G.size) * np.pi * temp;
    if sigma == None :
        sigma = error * G / np.linspace(1.001, 1., G.size)
    return (kn, G, sigma)

def writeResults (w, A, model, outfile) :
    if outfile.find(r'.npz') < 0 : outfile += '.npz'
    np.savez (outfile, A, w, model)

