"""
    @package SplineMaxEnt
    @file driver.py
    @author Steve Allen, CCS, Universite de Sherbrooke
    @date September 2011

    @brief This file contains a driver that reads the data and call the C++
    library to perform the analytical continuation of the data by maximum
    entropy.

    @note Python Modules required: sys, read_write, maxent
"""
#
# Copyright (C) 2011 Steve Allen
#
# This file is part of SplineMaxEnt.
# 
# SplineMaxEnt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SplineMaxEnt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SplineMaxEnt.  If not, see <http://www.gnu.org/licenses/>.
#
import sys
import os

class Usage(Exception) :
    def __init__(self, msg) :
        self.msg = msg

def init(cfgfile='param.xml', datafile=None, outfile=None) :
    """
        @brief Create the Maximum Entropy object
        @param cfgfile : name of the parameters file (param.xml)
        @param datafile : name of the file containing the data (None)
        @param outfile : name of the output file (None)
        @return the maxent object
    """
    cwd = os.getcwd()
    sys.path.append(cwd+'/pysrc')
    import read_write as io
    import maxent
    # Read the configuration file
    tagdico = {'data' : 'NodeList', 'output' : {'file' : 'str'}, \
        'maxent' : 'NodeList', 'iter' : 'NodeList', 'model' : 'NodeList'}
    answer = io.readCfgFile (cfgfile, tagdico)
    outfile2 = None
    for a in answer :
        typename = type(a).__name__
        if (typename == 'str' or typename == 'unicode') : outfile2 = a
        elif typename == 'NodeList' :
            dico = io.getxmlValues(a, 'file', 'nwcut', 'nmaxiter', 'name')
            if dico.has_key('file') : data_args = a
            elif dico.has_key('nwcut') : mem_args = a
            elif dico.has_key('nmaxiter') : iter_args = a
            elif dico.has_key('name') : model_args = a
            else : print "Argument ", a, " ignored\n"
        else :
            print "Argument ", a, " ignored\n"
    kn, G, sigma = io.readData (datafile, data_args)
    if outfile == None : outfile = outfile2
    if outfile == None : outfile = "spect.npz"
    if outfile.find(r'.npz') < 0 : outfile += '.npz'
    return maxent.maxent(kn, G, sigma, outfile, mem=mem_args, iterat=iter_args,\
        model=model_args, subset=True)

def main(argv=None) :
    """
        @brief Driver to execute Maxent for the analytical continuation of data
        @param argv : List of arguments
        Usage: driver.py -c cfgFile -d datafile -o outputfile
    """
    if argv == None : argv = sys.argv[1:]
    cfgfile = "param.xml"
    datafile = None
    outfile = None
    try :
        # Interpret the arguments
        if len(argv) > 1 :
            import getopt
            try :
                opts, args = getopt.getopt (argv, "hc:d:o:")
            except getopt.error, msg :
                raise Usage(msg)
            for opt, arg in opts :
                if opt == "-h" :
                    print __doc__
                    return 0
                if opt == "-c" : cfgfile = arg
                if opt == "-d" : datafile = arg
                if opt == "-o" : outfile = arg

        # Create the maxent object
        mem = init(cfgfile, datafile, outfile)

        # Perform the analytical continuation
        res = mem()

        if res < 0 : print "Stop because cannot go further\n"

    except Usage, err:
        print >>sys.stderr, err.msg
        print >>sys.stderr, "for help use -h"
        return 2
    
if __name__ == '__main__' :
    print "Command:"
    print " "*10, sys.argv
    print "Plateforme et version"
    print sys.platform, sys.version
    print "-"*30
    sys.exit(main())

