"""
    @package guiDriver.py
    @file maxent.py
    @author Steve Allen, CCS, Universite de Sherbrooke
    @date September 2011

    @brief This file contains a driver to launch the C++ library to perform
    the analytical continuation of the data by maximum entropy.

    @note Python Modules required: Tkinter, Numpy, Matplotlib
"""
#
# Copyright (C) 2011 Steve Allen
#
# This file is part of SplineMaxEnt.
# 
# SplineMaxEnt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SplineMaxEnt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SplineMaxEnt.  If not, see <http://www.gnu.org/licenses/>.
#

import Tkinter as tk
import matplotlib.pyplot as plt
import math
import numpy as np
import driver

class guiDriver(tk.Frame) :
    """
        @brief Class that create a graphical interface to do the maximum entropy
        It is based on tkinter.Frame
    """
    ##
    # Maximum entropy object
    mem = None

    def __init__(self, master=None) :
        """
            @brief Constructor of a guiDriver object
            @param self : handle to the object
            @param master : To create many levels of frame (optional)
        """
        tk.Frame.__init__(self, master)
        self.grid()
        self.createWidgets()

    def createWidgets(self) :
        """
            @brief Create the widget in the frame
            @param self : handle to the current object.
        """
        # Input Data
        self.cfgfileLabel = tk.Label(self, text="Configuration File")
        self.cfgfileLabel.grid()
        self.cfgfileVar = tk.StringVar()
        self.cfgfileVar.set("param.xml")
        self.cfgfileEnt = tk.Entry(self, textvariable=self.cfgfileVar)
        self.cfgfileEnt.grid()
        self.infileLabel = tk.Label(self, text="Input File Name")
        self.infileLabel.grid()
        self.infileVar = tk.StringVar()
        self.infileVar.set('data.dat')
        self.infileEnt = tk.Entry(self, textvariable=self.infileVar)
        self.infileEnt.grid()
        self.outLabel = tk.Label(self, text="Output File Name")
        self.outLabel.grid()
        self.outVar = tk.StringVar()
        self.outVar.set("spectrum.npz")
        self.outEnt = tk.Entry(self, textvariable=self.outVar)
        self.outEnt.grid()
        self.loadBut = tk.Button(self,text="Load Data",command=self.loadData)
        self.loadBut.grid()

        # Find the optimal solution
        self.alphaLabel = tk.Label(self, text="Weight of the entropy (Log10)")
        self.alphaLabel.grid()
        self.alphaVar = tk.Spinbox(self, from_=-2.0, to=25.0, increment=0.4)
        self.alphaVar.grid()
        self.nstopLabel = tk.Label(self, text="Stop after n iterations")
        self.nstopLabel.grid()
        self.nstopVar = tk.Spinbox(self, from_=0, to=25)
        self.nstopVar.grid()
        self.OptBut = tk.Button(self,text="Optimise",command=self.Optimise)
        self.OptBut.grid()

        # Quit Button
        self.quitBut = tk.Button(self, text="Quit", command=self.quit)
        self.quitBut.grid()

    def loadData(self) :
        """
            @brief Load the data that will be used for analytical continuation
            @param self : handle to the current object.
        """
        datafile = self.infileVar.get()
        cfgfile = self.cfgfileVar.get()
        outfile = self.outVar.get()
        self.mem = driver.init(cfgfile, datafile, outfile)
        alpha = math.log10(self.mem.getAlpha())
        alpha2 = float(self.alphaVar.get())
        while alpha - alpha2 > 0.2 :
            self.alphaVar.invoke('buttonup')
            alpha2 = float(self.alphaVar.get())

    def Optimise(self) :
        """
            @brief find the optimal spectrum.
            @param self : handle to the current object.
        """
        alpha = math.pow(10.0, float(self.alphaVar.get()))
        self.mem.setAlpha(alpha)
        niter = float(self.nstopVar.get())
        res = self.mem(niter)
        if res < 0 :
            print "Unable to go further in the optimissation process\n"
            print "You are probably stocked in a local minimum\n"
            print "You should restart with a different defaul model\n"
        alpha = math.log10(self.mem.getAlpha())
        alpha2 = float(self.alphaVar.get())
        while alpha2 - alpha > 0.2 :
            self.alphaVar.invoke('buttondown')
            alpha2 = float(self.alphaVar.get())
        A = self.mem.getSpectrum()
        w = self.mem.getFrequencies()
        model = self.mem.getModel()
        if (A != None and w != None) :
            plt.semilogx(w, A, 'r', label='Best')
            plt.semilogx(w, model, 'b', label='model')
            plt.legend()
            plt.xlabel("Frequency")
            plt.ylabel("Spectrum")
            plt.show()

app = guiDriver()
app.master.title("Analytical Continuation by MaxEnt")
app.mainloop()

