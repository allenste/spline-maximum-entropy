"""
    @package SplineMaxEnt
    @file maxent.py
    @author Steve Allen, CCS, Universite de Sherbrooke
    @date September 2011

    @brief This file contains a driver to launch the C++ library to perform
    the analytical continuation of the data by maximum entropy.

    @note Python Modules required:
"""
#
# Copyright (C) 2011 Steve Allen
#
# This file is part of SplineMaxEnt.
# 
# SplineMaxEnt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SplineMaxEnt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SplineMaxEnt.  If not, see <http://www.gnu.org/licenses/>.
#

import scipy.special as spsp
import numpy as np
import math
import read_write as io

class maxent(object) :
    """
        @brief Class to build the object containing the data and to perform
        the analytical continuation
    """
    ##
    # Matsubara frequencies
    kn = None

    ##
    # Green's function
    G = None

    ##
    # Precision of the Green's function
    sigma = None

    ##
    # Filename where to save the results
    outfile = None

    ##
    # Weight of the entropy
    alpha = 1.e18

    ##
    # Default model
    model = None

    ##
    # Number of values for alpha
    nalpha = 1

    ##
    # Maximum number of iterations for a given alpha
    nmaxiter = 1

    ##
    # Tolerance on chi2 to stop the iterations
    tol_chi2 = 0

    ##
    # Print result after that amount of iterations
    niterpr = 1

    ##
    # Modify alpha after each iteration
    alphadiv = 0.25

    ##
    # Ratio for the evolution of the model
    ratio = 0.

    ##
    # Nb of iterations done on the list of alpha
    alpha_iter = 0

    ##
    # Handle for the function to call
    c_fct = None

    ##
    # Real frequencies
    w = None

    ##
    # Number of frequencies before the cut
    nwcut = 0

    ##
    # Spectrum
    A = None

    def buildFrequencies(self, nw, nwcut, wcut) :
        """
            @brief This function build the grid of real frequencies
            @param self : handle to the current object
            @param nw : Total number of frequencies
            @param nwcut : Number of frequencies before the change of spline
            @param wcut : Value of the frequency where the change appears
            @note Based on the Frequency grid by Dominic Bergeron
        """
        nwhf = nw - nwcut
        delta = 1.0 / ((nwhf + 1) * wcut) 
        self.nwcut = nwcut
        self.w = np.hstack((np.linspace(0.0, wcut, nwcut), \
               1.0 / np.linspace(nwhf * delta, delta, nwhf)))

    def parseMaxentArgs(self, mem) :
        """
            @brief This fucntion parses the list of parameters for the maximum
            entropy method.
            @param self : handle to the current object
            @param mem : NodeList containing the list of parameters
        """
        dico = io.getxmlValues(mem, "nw", "nwcut", "wcut", "alpha")
        nwcut = int(dico.get("nwcut", -1))
        nw = int(dico.get("nw", 2 * nwcut - 1))
        if nwcut < 0 :
            if nw > 0 :
                nw = (nw / 2) * 2 + 1
                nwcut = (nw + 1) / 2
            else :
                nw = 501
                nwcut = 251
        wcut = float(dico.get("wcut", 0.01 * (nwcut - 1)))
        self.alpha = float(dico.get("alpha", self.alpha))
        self.buildFrequencies(nw, nwcut, wcut)

    def parseIterArgs(self, iterat) :
        """
            @brief This function parses the list of parameters for an iterative
            approach for the analytical continuation
            @param self : handle to the current object
            @param iterat : NodeList containing the list of parameters
        """
        dico = io.getxmlValues(iterat, "nalpha", "niterpr", "alphadiv", \
            "modelratio", "nmaxiter", "tolchi2")
        self.nalpha = int(dico.get("nalpha", self.nalpha))
        self.niterpr = int(dico.get("niterpr", self.niterpr))
        self.alphadiv = np.power(10.,float(dico.get("alphadiv",self.alphadiv)))
        self.ratio = float(dico.get("modelratio", self.ratio))
        self.nmaxiter = int(dico.get("nmaxiter", self.nmaxiter))
        self.tol_chi2 = int(dico.get("tol_chi2", self.tol_chi2))

    def buildModel(self, model) :
        """
            @brief This function build the model used to compute the entropy
            @param self : handle to the current object
            @param model : NodeList descritption of the parameters used to
            build the model
        """
        dico = io.getxmlValues(model, "name", "mean", "stddev", "exponent")
        name = dico.get("name", "gen_normal")
        mean = float(dico.get("mean", 0.))
        stddev = min(float(dico.get("stddev", 10.)), self.w[-1]*0.1)
        exponent = float(dico.get("exponenent", 4.))
        if name == "gen_normal" :
            mod_min = 1.e-12
            self.model = np.maximum(mod_min, np.exp(-1*np.power(np.absolute( \
                self.w)/stddev, exponent))*0.5*exponent/stddev/spsp.gamma(1. \
                /exponent))
        else :
            self.model = np.ones((self.w.size,), dtype=np.float64) / self.w.size

    def loadLib(self) :
        """
            @brief This function load the library containing the C function.
            @param self : handle to the current object
        """
        dtype = np.float64
        import ctypes
        import numpy.ctypeslib as npcl
        _libmaxent = npcl.load_library("libmaxent.so", ".")
        if self.G.dtype.name.find('complex') >= 0 :
            self.c_fct = _libmaxent.CmaxentCmplx
        else : self.c_fct = _libmaxent.CmaxentReal
        self.c_fct.restype = ctypes.c_double
        self.c_fct.argtypes = [npcl.ndpointer(ndim=1,dtype=np.float64), \
            npcl.ndpointer(ndim=1,dtype=np.float64), \
            npcl.ndpointer(ndim=1,dtype=np.float64), ctypes.c_int, \
            npcl.ndpointer(ndim=1,dtype=np.float64), ctypes.c_int, \
            ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_double, \
            npcl.ndpointer(ndim=1,dtype=np.float64), \
            npcl.ndpointer(ndim=1,dtype=np.float64), ctypes.c_int]

    def __init__(self, kn, G, sigma, outfile, **kwargs) :
        """
            @brief Function to build the object
            @param self : Handle for the object
            @param kn : Numpy array containing the Matsubara frequencies
            @param G : Numpy array containing the Green's function
            @param sigma : Numpy array containing the precision of the green fct
            @param kwargs : Optional additional arguments (mem, iterat, model,
            subset)
        """
        self.outfile = outfile
        subset = kwargs.get("subset", False)
        if subset :
            r = 4
            Nl = (kn.size - 1) / np.power(2, r)
            Nm = Nl / 2;
            Nh = r * Nm + 1
            lj = np.arange(Nh) / Nm
            nh = np.mod(np.arange(Nh), Nm) * np.power(2, lj+1)
            nh = (nh + Nl * np.power(2, lj)).tolist()
            n = range(Nl) + nh
            self.kn = kn[n]
            self.G = G[n]
            self.sigma = sigma[n]
        else :
            self.kn = kn
            self.G = G
            self.sigma = sigma
        mem = kwargs.get("mem", None)
        self.parseMaxentArgs(mem)
        iterat = kwargs.get("iterat", None)
        self.parseIterArgs(iterat)
        model = kwargs.get("model", None)
        self.buildModel(model)
        self.loadLib()

    def __call__(self, iterbreak=None) :
        """
            @brief This function performs the analytical continutation
            @param self : Handle to the current object
            @param iterbreak : Number of iterations on alpha before stopping.
        """
        isAinit = 1
        if self.A == None :
            self.A = self.model.copy()
            isAinit = 0
        previter = self.alpha_iter
        if iterbreak == None : iterbreak = self.nalpha - previter
        while (self.alpha_iter<self.nalpha and self.alpha_iter-previter< \
            iterbreak) :
            chi2=self.c_fct(self.G, self.sigma, self.kn, self.kn.size, self.w,\
            self.w.size, self.nwcut, 1, 0, self.alpha, self.model, self.A, isAinit)
            isAinit = 1
            niter = 1
            while True :
                chi2prec = chi2
                chi2=self.c_fct(self.G,self.sigma,self.kn,self.kn.size,self.w,\
                    self.w.size,self.nwcut,1,0,self.alpha,self.model,self.A,1)
                niter += 1
                if (math.fabs(chi2-chi2prec)/chi2 <= self.tol_chi2 or \
                    niter > self.nmaxiter) : break
            if (self.nalpha > 1 and (self.alpha_iter+1)%self.niterpr==0) :
                l = self.outfile.find(r".npz")
                filename = self.outfile[:l] + str(self.alpha_iter+1) + '.npz'
                io.writeResults(self.w, self.A, self.model, filename)
            elif self.alpha_iter == self.nalpha :
                io.writeResults(self.w, self.A, self.model, self.outfile)
            self.alpha_iter += 1
            self.alpha /= self.alphadiv
            if (self.ratio > 0. and self.ratio < 1.) :
                self.model = self.model * (1.-self.ratio) + self.A * self.ratio
            elif self.ratio >= 1. : self.model = self.A
        return 0;

    def getModel(self) :
        """
            @brief This function returns the model used in the entropy
            @param self : Handle to the current object
            @return Ndarray containing the model
        """
        return self.model

    def getFrequencies(self) :
        """
            @brief This function returns the frequency grid
            @param self : Handle to the current object
            @return Ndarray containing the frequency grid
        """
        return self.w

    def getSpectrum(self) :
        """
            @brief This function returns the spectrum
            @param self : Handle to the current object
            @return Ndarray containing the spectrum
        """
        return self.A

    def getAlpha(self) :
        """
            @brief This function returns the weight of the entropy
            @param self : Handle to the current object
            @return the weight of the entropy
        """
        return self.alpha

    def setAlpha(self, alpha) :
        """
            @brief This function sets the weight of the entropy
            @param self : Handle to the current object
            @param alpha : the weight of the entropy
        """
        self.alpha = alpha

