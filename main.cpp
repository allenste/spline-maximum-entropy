/*!
    \file main.cpp
    \author Steve Allen, CCS, Universite de Sherbrooke
    \date September 2011

    \brief This file contains a program to call the maximum entropy function.
*/
/*
 * Copyright (C) 2011 Steve Allen
 *
 * This file is part of SplineMaxEnt.
 * 
 * SplineMaxEnt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SplineMaxEnt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SplineMaxEnt.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "energieLibre.hpp"
#include "maxent.hpp"
#include <cmath>
#include <cstdio>
#include <fstream>
#include <sstream>
#include <iostream>

int main(int argc, char **argv) {
    // Initialize
    // Input file
    char datafile[] = "data.dat";
    // Output file
    char outfile[] = "res";
    // alpha gives the weight of the regularization function (entropy)
    double alpha = 1.e18; // Initial value
    double alphadiv = pow(10.0, -0.1); // change of value
    int nalphaIter = 156; // maximum number of values for alpha
    int niterpr = 10;     // print the result at each multiple of this number
    int Niter_alpha = 20; // number of iteration for a given alpha
    // real frequency grid
    int nwcut = 301; 	// number of frequencies in the regular stepping
    double wcut = 0.3;  // at what values the stepping change
    int nw = 2 * nwcut - 1;  // Don't change that line
    // precision on the data.
    double error  = 1.e-6;
    // precision for convergence
    double tolchi2 = 1e-6;
    // parameter for the selection of Matsubara frequencies (subset sampling)
    int r = 4;
    // Temperature
    double T = 0.1;

    // Get the number of rows and columns in the file
    std::ostringstream *s = new std::ostringstream();
    *s << "wc -l " << datafile;
    FILE *answer = popen(s->str().c_str(), "r");
    int nkn;
    int status = fscanf(answer, "%d", &nkn);
    pclose(answer);
    delete s;
    s = new std::ostringstream();
    *s << "wc -l " << datafile;
    answer = popen(s->str().c_str(), "r");
    int nword;
    status = fscanf(answer, "%d", &nword);
    pclose(answer);
    delete s;
    int ncol = nword / nkn;

    // Read the data
    double *G_in = new double[nkn];
    double *sigma_in = new double[nkn];
    double cst = 2 * T * 3.14159265358979;
    double buf;
    std::ifstream *in = new std::ifstream(datafile);
    if (ncol > 2) {
        for (int i=0; i<nkn; ++i) *in >> buf >> G_in[i] >> sigma_in[i];
    } else {
        for (int i=0; i<nkn; ++i) *in >> buf >> G_in[i];
        for (int i=0; i<nkn; ++i)
            sigma_in[i] = error * G_in[i]; // / (1.001-.001*i/(nkn-1));
    }
    delete in;

    // Generate a subset of data
    int Nl = (nkn-1) / ((int) pow(2.0, r));
    int Nm = Nl / 2;
    int Nh = r * Nm + 1;
    nkn = Nl + Nh;
    double *kn = new double[nkn];
    double *G = new double[nkn];
    double *sigma = new double[nkn];
    for (int i=0; i<Nl; ++i) {
        kn[i] = cst * i;
        G[i] = G_in[i];
        sigma[i] = sigma_in[i];
    }
    for (int j=0; j<Nh; ++j) {
        int lj = j / Nm;
        int nh = Nl * ((int) pow(2., lj)) + (j % Nm) * ((int) pow(2., lj+1));
        kn[Nl+j] = cst * nh;
        G[Nl+j] = G_in[nh];
        sigma[Nl+j] = sigma_in[nh];
    }
    delete [] sigma_in;
    delete [] G_in;

    // Generate the frequencies
    double *w = new double[nw];
    double delta = wcut / (nwcut - 1);
    for (int i=0; i<nwcut; ++i) w[i] = i * delta;
    int nwhf = nw - nwcut + 1;
    delta = 1. / w[nwcut-1] / nwhf;
    for (int i=1; i<nwhf; ++i) w[nw-i] = 1.0 / (i * delta);

    // Generate the default model
    double *def = new double[nw];
    double mindef = 1.e-12;
    double D = (100. > w[nw-1] ? w[nw-1]*0.1 : 10.);
    int p = 4;
    for (int i=0; i<nw; ++i) {
        def[i] = exp(-1*pow(w[i]/D, p)) + mindef;
    }

    // Generate the object for optimisation
    energieLibreObj<double> *e = new energieLibreObj<double>(kn, G, sigma, nkn,
        w, nw, nwcut, true, false, alpha, def);
    delete [] sigma;
    delete [] G;
    delete [] kn;

    // Perform the minimisation
    double *A = new double[nw];
  //  cst = 1.0 / exp(1.0);
    for (int i=0; i<nw; ++i) A[i] = def[i];
    double relError = e->ComputeRelativeError(A);
    for (int i=0; i<nalphaIter; ++i) {
        double chi2 = maxent<double>(alpha, A, def, true, e);
        #if VERBOSE >= 1
        std::cout<<'\n';
        #endif
        int iter_alpha=1;
        double chi2prec;
        do 
        {
            chi2prec = chi2;
            chi2 = maxent<double>(alpha, A, def, true, e);
            #if VERBOSE >= 1
            std::cout<<'\n';
            #endif
            iter_alpha++;
        } while (fabs(chi2-chi2prec)/chi2>tolchi2 && iter_alpha<Niter_alpha);
        double prevRelError = relError;
        relError = e->ComputeRelativeError(A);
        std::cout << "Relative Error on Chi=" << relError << "\n\n\n";
        if ((prevRelError-relError)/relError<tolchi2 and chi2<(double) nkn)
            nalphaIter = i + 1;
        if ((i+1)%niterpr == 0 || (i+1) == nalphaIter) {
            s = new std::ostringstream();
            s->precision(3);
            *s << outfile << alpha << ".dat";
            std::ofstream *out = new std::ofstream(s->str().c_str());
            delete s;
            for (int i=0; i<nw; ++i) {
                *out << w[i] << "  " << A[i] << "  " << def[i] << '\n';
            }
            delete out;
        }
        alpha *= alphadiv;
        e->setWeight(alpha);
    }

    // Save the data
    delete [] A;
    delete e;
    delete [] def;
    delete [] w;
    return 0;
}

