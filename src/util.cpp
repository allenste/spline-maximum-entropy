/*!
    \file util.cpp
    \author Steve Allen, CCS, Universite de Sherbrooke
    \date August 2011

    \brief This file defines some useful mathematicla functions
*/
/*
 * Copyright (C) 2011 Steve Allen
 *
 * This file is part of SplineMaxEnt.
 * 
 * SplineMaxEnt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SplineMaxEnt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SplineMaxEnt.  If not, see <http://www.gnu.org/licenses/>.
**/
#include "util.h"
#ifdef USE_BLAS
#include "blas.h"
#endif
#ifdef USE_LAPACK
#include "blas.h"
#endif
#include <iostream>
#include <limits>
#include <cmath>
#include <cstdlib>

/*!
    \brief Normalise a vector if its norm is non zero
    \param v1: vector to normalise. It is normalize on output. (inout)
    \param m: Number of elements in the vector (in)
*/
void normalize(double *v1, int m) {
    const double eps = std::numeric_limits<double>::epsilon();
#ifdef USE_BLAS
    int inc = 1;
    double nrm = dnrm2_(&m, v1, &inc);
    if (nrm > eps) {
        nrm = 1.0 / nrm;
        dscal_(&m, &nrm, v1, &inc);
    }
#else
    double norm = 0.;
    for (int i=0; i<m; ++i) norm += v1[i] * v1[i];
    norm = sqrt(norm);
    if (norm > eps) {
        for (int i=0; i<m; ++i) v1[i] /= norm;
    }
#endif
}

/*!
    \brief Fonction that returns a normalise version of a vector
    \param v1: vector to normalise (in)
    \param m: number of elements in v1 (in)
    \param v2: normalised vector (or zero if norm=0). Must be at least as
        tall as the vector v1. (out; allocated on input)
*/
void normalize(double *v1, int m, double *v2) {
    const double eps = std::numeric_limits<double>::epsilon();
#ifdef USE_BLAS
    int inc = 1;
    double nrm = dnrm2_(&m, v1, &inc);
    for (int i=0; i<m; ++i) v2[i] = 0;
    if (nrm > eps) {
        nrm = 1.0 / nrm;
        daxpy_(&m, &nrm, v1, &inc, v2, &inc);
    }
#else
    double norm = 0.;
    for (int i=0; i<m; ++i) norm += v1[i] * v1[i];
    norm = sqrt(norm);
    if (norm > eps) {
        for (int i=0; i<m; ++i) v2[i] = v1[i] / norm;
    } else {
        for (int i=0; i<m; ++i) v2[i] = 0.;
    }
#endif
}

/*!
    \brief Apply a Gram-Shmith orthogonalisation on v2 to make sure it is
        orthogonal to v1.
    \param v1 : first vector (in)
    \param v2: second vector that is make orthogonal on output. (inout)
    \param m: number of element considered for the orthogonalisation. v1
        and v2 must be at least that size (in).
*/
void orthogonalize(double *v1, double *v2, int m) {
#ifdef USE_BLAS
    int inc = 1;
    double sp = ddot_(&m, v1, &inc, v2, &inc);
    sp *= -1;
    daxpy_(&m, &sp, v1, &inc, v2, &inc);
#else
    double scalarprod = 0;
    for (int i=0; i<m; ++i) scalarprod += v1[i] * v2[i];
    for (int i=0; i<m; ++i) v2[i] -= v1[i] * scalarprod;
#endif
}

/*!
    \brief multiply a matrix by the transpose of a second matrix
    \param A first matrix (in)
    \param B second matrix (in)
    \param C results: C = A * B' (out; allocated on input)
    \param n number of rows of A, also the number of rows of C (in)
    \param m number of columns of A, also the number of columns of B (in)
    \param l number of rows of B, also the number of columns of C (in)
*/
void matrixMult(double **A, double **B, double **C, int n, int m, int l) {
#ifdef USE_BLAS
    char ta = 'N';
    char tb = 'T';
    double a = 1.;
    double b = 0.;
    dgemm_(&tb, &ta, &l, &n, &m, &a, B[0], &m, A[0], &m, &b, C[0], &l);
#else
    for (int i=0; i<n; ++i) {
        for (int j=0; j<l; ++j) {
            double v = 0.;
            for (int k=0; k<m; ++k) v += A[i][k] * B[j][k];
            C[i][j] = v;
        }
    }
#endif
}

/*!
    \brief multiply a matrix by the transpose of a second matrix and add to
        a third matrix.
    \param A first matrix (in)
    \param B second matrix (in)
    \param C results: C = A * B' + C (inout)
    \param n number of rows of A, also the number of rows of C (in)
    \param m number of columns of A, also the number of columns of B (in)
    \param l number of rows of B, also the number of columns of C (in)
*/
void matrixMultAdd(double **A, double **B, double **C, int n, int m, int l) {
#ifdef USE_BLAS
    char ta = 'N';
    char tb = 'T';
    double a = 1.;
    double b = 1.;
    dgemm_(&tb, &ta, &l, &n, &m, &a, B[0], &m, A[0], &m, &b, C[0], &l);
#else
    for (int i=0; i<n; ++i) {
        for (int j=0; j<l; ++j) {
            double v = 0.;
            for (int k=0; k<m; ++k) v += A[i][k] * B[j][k];
            C[i][j] += v;
        }
    }
#endif
}

/*!
    \brief Returns the eignevalues in ascending order with their
        corresponding normalized eigenvectors.
    \param A: matrix to compute eigenvalues and eigenvectors (in).
    \param n: size of the matrix (in).
    \param V: Matrix of eigenvectors (out; allocated on input).
    \param D: Vector of eigenvalues (out; allocated on input).
*/
void eigen(double *A, int n, double *V, double *D) {
    const double eps = std::numeric_limits<double>::epsilon();
    if (n == 1) {
        D[0] = A[0];
        V[0] = 1.;
    } else if (n == 2) {
        double a = (A[0] - A[3]) * 0.5;
        a = sqrt(a*a + A[1]*A[2]);
        double b = (A[0] + A[3]) * 0.5;
        double c = b - a;
        double d = b + a;
        double v1[2], v2[2];
        v1[0] = A[1];
        v1[1] = c - A[0];
        v2[0] = d - A[3];
        v2[1] = A[2];
        if (c < a * 1.e-10) {
            if (A[3] > A[0]) {
                c = A[0] - A[1]*A[2] / (A[3] - A[0]);
                v1[1] = c - A[0];
                v2[0] = A[1]*A[2] / (A[3] - A[0]);
            } else {
                c = A[3] - A[1]*A[2] / (A[0] - A[3]);
            }
        }
        double nrm1 = v1[0]*v1[0] + v1[1] * v1[1];
        double nrm2 = v2[0]*v2[0] + v2[1] * v2[1];
        if (nrm1 > eps && nrm2 > eps) {
            nrm1 = 1.0 / sqrt(nrm1);
            nrm2 = 1.0 / sqrt(nrm2);
            v1[0] *= nrm1;
            v1[1] *= nrm1;
            v2[0] *= nrm2;
            v2[1] *= nrm2;
        } else if (nrm1 > eps) {
            nrm1 = 1.0 / sqrt(nrm1);
            v1[0] *= nrm1;
            v1[1] *= nrm1;
            if (v1[0] > v1[1]) {
                v2[0] = 0.;
                v2[1] = 1.;
            } else {
                v2[0] = 1.;
                v2[1] = 0.;
            }
        } else if (nrm2 > eps) {
            nrm2 = 1.0 / sqrt(nrm2);
            v2[0] *= nrm2;
            v2[1] *= nrm2;
            if (v2[0] > v2[1]) {
                v1[0] = 0.;
                v1[1] = 1.;
            } else {
                v1[0] = 1.;
                v1[1] = 0.;
            }
        } else {
            v1[0] = 1.;
            v1[1] = 0.;
            v2[0] = 0.;
            v2[1] = 1.;
        }
        if (c < d) {
            D[0] = c;
            D[1] = d;
            V[0] = v1[0];
            V[1] = v2[0];
            V[2] = v1[1];
            V[3] = v2[1];
        } else {
            D[0] = d;
            D[1] = c;
            V[0] = v2[0];
            V[1] = v1[0];
            V[2] = v2[1];
            V[3] = v1[1];
        }
        #ifdef _TEST
        double det = A[0] * A[3] - A[1] * A[2];
        double diff = fabs((A[0] - D[0]) * (A[3] - D[0]) - A[1] * A[2]);
        double test = (D[0] < det ? det : D[0]);
        if (diff > 1.e-5 * test) std::cout<<"Warning first eigenvalue is not good\n";
        diff = fabs((A[0] - D[1]) * (A[3] - D[1]) - A[1] * A[2]);
        test = (D[1] < det ? det : D[1]);
        if (diff > 1.e-5 * test) {
            std::cout<<"Warning second eigenvalue is not good\n";
            std::cout<<A[0]<<" "<<A[1]<<" "<<A[2]<<" "<<A[3]<<" "<<D[1]<<" "<<det<<'\n';
        }
        diff = fabs((A[0] - D[0]) * V[0] + A[1] * V[2]);
        if (diff > 1.e-5 * det) std::cout<<"Warning first component for first eigenvector\n";
        diff = fabs(A[2] * V[0] + (A[3] - D[0]) * V[2]);
        if (diff > 1.e-5 * det) std::cout<<"Warning second component for first eigenvector\n";
        diff = fabs((A[0] - D[1]) * V[1] + A[1] * V[3]);
        if (diff > 1.e-5 * det) std::cout<<"Warning first component for second eigenvector\n";
        diff = fabs(A[2] * V[1] + (A[3] - D[1]) * V[3]);
        if (diff > 1.e-5 * det) std::cout<<"Warning second component for second eigenvector\n";
        #endif
    } // End eif (n == 2)
    else {
        #ifdef USE_LAPACK
        char job = 'V';
        char uplo = 'U';
        int info, inc=1, m=n*n, lwork=1+6*n+2*m, liwork=3+5*n;
        double *work = new double[lwork];
        int *iwork = new int[liwork];
        dcopy_(&n, A, &inc, V, &inc);
        dsyevd_(&job, &uplo, &n, V, &n, D, work, &lwork, iwork, &liwork, &info);
        if (info != 0) {
            std::cout<<"Error in eigen\n";
        }
        #else
        std::cout<<"Not equipped to compute eigenvectors of this matrix\n";
        std::cout<<"Compile with USE_LAPACK\n";
        exit(-1);
        #endif
    }
}

/*!
    \brief Function that inverses a general square matrix.
    \param A :  Double array that contains the element of the matrix (inout).
    \param n : size of the matrix (in).
*/
void inverseMatrix(double **A, int n)
{
#ifdef USE_LAPACK
    int info;
    int *ipiv = new int[n];
    dgetrf_(&n, &n, A[0], &n, ipiv, &info);
    int lwork = -1;
    double wsize;
    dgetri_(&n, A[0], &n, ipiv, &wsize, &lwork, &info);
    lwork = (int) wsize;
    double *work = new double[lwork];
    dgetri_(&n, A[0], &n, ipiv, work, &lwork, &info);
    delete [] work;
    delete [] ipiv;
#else
    return;
#endif
}

