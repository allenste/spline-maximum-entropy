/*!
    \file maxent.cpp
    \author Steve Allen, CCS, Universite de Sherbrooke
    \date September 2011

    \brief This file contains a function that find the spectrum to minimize the
    cost of a likelihood function plus an entropy function.
*/
/*
 * Copyright (C) 2011 Steve Allen
 *
 * This file is part of SplineMaxEnt.
 * 
 * SplineMaxEnt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SplineMaxEnt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SplineMaxEnt.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "energieLibre.h"
#include "fmincon.h"
#include <complex>
#include <iostream>

static energieLibreObj<double> *e_real = NULL;
static energieLibreObj<std::complex<double> > *e_cmplx = NULL;
static Optimize *o = NULL;

/*!
    \brief Function that find the spectrum that minimizes a cost function
        corresponding to a given Matsubara Green's function.
    \param G : Array containing the Matubara Green's function (in)
    \param sigma : Array of the fluctuations of the Green's function (in).
    \param kn : Array of the corresponding Matsubara frequencies (in).
    \param nkn : Number of Matsubara frequencies (in)
    \param w : Array of real frequencies (in)
    \param nw : Number of real frequencies (in).
    \param nw_spline : Number of frequencies after which the type of spline
        change (in).
    \param sym : Is the spectrum symmetric? (in)
    \param fermions : Do you deal with fermions (or bosons)? (in)
    \param alpha : respective weight of the entropy in the cost function (in).
    \param A ; Spectrum (out; allocated on input and may be initialized).
    \param def : Array containing the model for the entropy (in).
    \param isAinitialized : is A initialized?
    \return The likelihood value
*/
template<class T> double maxent(T *G, T *sigma, double *kn, int nkn, double *w,
    int nw, int nw_spline, bool sym, bool fermions, double alpha, double *A,
    double *def, bool isAInitialized)
{
    // Generate the object to compute the Cost
    energieLibreObj<T> *e = new energieLibreObj<T>(kn, G, sigma, nkn, w, nw,
        nw_spline, sym, fermions, alpha, def);

    // Initialize the spectrum, gradient and Hessian
    double *grad = new double[nw];
    double *H = new double[nw*nw];
    double cost = e->initiliazeSpect(A, grad, H, isAInitialized);

    // Get the limits for the search
    double *l;
    double *u;
    int n;
    e->GetLimits(&l, &u, &n);

    // Build the minimization algorithm
    o = new Optimize(n, l, u, A, cost, grad, H);

    // Get the extra arguments for the cost function
    argCostType argCost = e->GetArgumentCost();

    // Procede with the minimization
    o->Driver(&energieLibre, &argCost);

    // Print the result
#if VERBOSE >= 1
    std::cout<<"alpha=" << alpha << '\n';
    std::cout<<"chi2="<< e->ComputeLikelihood(A)<<'\n';
    o->PrintStatistics();
#endif
    double chi2 = e->ComputeLikelihood(A);

    // Clean the memory
    l = NULL;
    u = NULL;
    grad = NULL;
    H = NULL;
    delete o;
    delete e;
    o = NULL;
    e = NULL;
    return chi2;
}

/*!
    \brief Function that find the spectrum that minimizes a cost function
        corresponding to a given Matsubara Green's function.
    \param alpha : respective weight of the entropy in the cost function (in).
    \param A ; Spectrum (out; allocated on input and may be initialized).
    \param def : Array containing the model for the entropy (in).
    \param isAinitialized : is A initialized?
    \param eptr : Pointer to the Free-Energy object
    \param optr : Pointer to the optimisation object.
    \return The likelihood value
*/
template<class T> double maxent(double alpha, double *A, double *def,
    bool isAInitialized, energieLibreObj<T> *e)
{
    // If the optimization object is not built, build it.
    if (o == NULL) {
        int nw = e->getNumberFreq();
        double *grad = new double[nw];
        double *H = new double[nw*nw];
        double cost = e->initiliazeSpect(A, grad, H, isAInitialized);
        double *l;
        double *u;
        int n;
        e->GetLimits(&l, &u, &n);
        o = new Optimize(n, l, u, A, cost, grad, H);
        l = NULL;
        u = NULL;
        grad = NULL;
        H = NULL;
    } else {
        double *grad = o->getGradPtr();
        double *H = o->getHessianPtr();
        double cost = e->initiliazeSpect(A, grad, H, isAInitialized);
        o->SetInitialPoint(A, cost, grad, H);
    }

    // Get the extra arguments for the cost function
    argCostType argCost = e->GetArgumentCost();

    // Procede with the minimization
    o->Driver(&energieLibre, &argCost);

    // Print the result and final Likelihood
#if VERBOSE >= 1
    std::cout<<"alpha=" << alpha << '\n';
    std::cout<<"chi2="<< e->ComputeLikelihood(A)<<'\n';
    o->PrintStatistics();
#endif

    return e->ComputeLikelihood(A);
}

// C interface function to call maxent with real or complex data
extern "C" {
/*!
    \brief C interface to call maxent for the real case.
    \param G : Array containing the Matubara Green's function (in)
    \param sigma : Array of the fluctuations of the Green's function (in).
    \param kn : Array of the corresponding Matsubara frequencies (in).
    \param nkn : Number of Matsubara frequencies (in)
    \param w : Array of real frequencies (in)
    \param nw : Number of real frequencies (in).
    \param nw_spline : Number of frequencies after which the type of spline
        change (in).
    \param sym : Is the spectrum symmetric? (in)
    \param fermions : Do you deal with fermions (or bosons)? (in)
    \param alpha : respective weight of the entropy in the cost function (in).
    \param A ; Spectrum (out; allocated on input and may be initialized).
    \param def : Array containing the model for the entropy (in).
    \param isAinitialized : is A initialized?
    \return The likelihood value
*/
double CmaxentReal(double *G, double *sigma, double *kn, int nkn, double *w,
    int nw, int nw_spline, int sym, int fermions, double alpha, double *def,
    double *A, int isAInitialized)
{
    bool isAinit = (isAInitialized > 0);
    if (e_real == NULL) {
        bool fermi = (fermions > 0);
        bool s = (sym > 0);
        e_real = new energieLibreObj<double>(kn, G, sigma, nkn, w, nw,
            nw_spline, s, fermi, alpha, def);
    } else {
        e_real->setWeight(alpha);
        e_real->setModel(def);
    }
    return maxent<double>(alpha, A, def, isAinit, e_real);
}

/*!
    \brief C interface to call maxent for the complex case.
    \param G : Array containing the Matubara Green's function (in)
    \param sigma : Array of the fluctuations of the Green's function (in).
    \param kn : Array of the corresponding Matsubara frequencies (in).
    \param nkn : Number of Matsubara frequencies (in)
    \param w : Array of real frequencies (in)
    \param nw : Number of real frequencies (in).
    \param nw_spline : Number of frequencies after which the type of spline
        change (in).
    \param sym : Is the spectrum symmetric? (in)
    \param fermions : Do you deal with fermions (or bosons)? (in)
    \param alpha : respective weight of the entropy in the cost function (in).
    \param A ; Spectrum (out; allocated on input and may be initialized).
    \param def : Array containing the model for the entropy (in).
    \param isAinitialized : is A initialized?
    \return The likelihood value
*/
double CmaxentCmplx(double *G, double *sigma, double *kn, int nkn, double *w,
    int nw, int nw_spline, int sym, int fermions, double alpha, double *def,
    double *A, int isAInitialized)
{
    bool isAinit = (isAInitialized > 0);
    if (e_cmplx == NULL) {
        bool fermi = (fermions > 0);
        bool s = (sym > 0);
        std::complex<double> *Gc = new std::complex<double>[nkn];
        std::complex<double> *sigmac = new std::complex<double>[nkn];
        for (int i=0; i<nkn; ++i) {
            Gc[i] = std::complex<double>(G[2*i], G[2*i+1]);
            sigmac[i] = std::complex<double>(sigma[2*i], sigma[2*i+1]);
        }
        e_cmplx = new energieLibreObj<std::complex<double> >(kn, Gc, sigmac,
            nkn, w, nw, nw_spline, s, fermi, alpha, def);
        delete [] sigmac;
        delete [] Gc;
    } else {
        e_cmplx->setWeight(alpha);
        e_cmplx->setModel(def);
    }
    return maxent<std::complex<double> >(alpha, A, def, isAinit, e_cmplx);
}

/*!
    \brief C interface to call maxent for the real case (Iterations).
    \param alpha : respective weight of the entropy in the cost function (in).
    \param A ; Spectrum (out; allocated on input and may be initialized).
    \param def : Array containing the model for the entropy (in).
    \param isAinitialized : is A initialized?
    \return The likelihood value
*/
double CmaxentRealIter(double alpha, double *def, double *A, int isAInitialized)
{
    bool isAinit = (isAInitialized > 0);
    if (e_real == NULL) {
#if VERBOSE >= 0
        std::cout << "Error the Free Energy object has not been called\n";
        std::cout << "You need to call the function with more arguments ";
        std::cout << "(kn, G, ...)\n";
#endif
        return 0.;
    }
    e_real->setWeight(alpha);
    e_real->setModel(def);
    return maxent<double>(alpha, A, def, isAinit, e_real);
}

/*!
    \brief C interface to call maxent for the complex case (Iterations).
    \param alpha : respective weight of the entropy in the cost function (in).
    \param A ; Spectrum (out; allocated on input and may be initialized).
    \param def : Array containing the model for the entropy (in).
    \param isAinitialized : is A initialized?
    \return The likelihood value
*/
double CmaxentCmplxIter(double alpha,double *def,double *A,int isAInitialized)
{
    bool isAinit = (isAInitialized > 0);
    if (e_cmplx == NULL) {
#if VERBOSE >= 0
        std::cout << "Error the Free Energy object has not been called\n";
        std::cout << "You need to call the function with more arguments ";
        std::cout << "(kn, G, ...)\n";
#endif
        return 0.;
    }
    e_cmplx->setWeight(alpha);
    e_cmplx->setModel(def);
    return maxent<std::complex<double> >(alpha, A, def, isAinit, e_cmplx);
}

}

