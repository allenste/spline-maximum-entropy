/*!
    \file energieLibre.cpp
    \author Steve Allen, CCS, Universite de Sherbrooke
    \date September 2011

    \brief This file contains a class to build an object to compute the cost
    corresponding to a given spectrum.
*/
/*
 * Copyright (C) 2011 Steve Allen
 *
 * This file is part of SplineMaxEnt.
 * 
 * SplineMaxEnt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SplineMaxEnt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SplineMaxEnt.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "energieLibre.h"
#ifdef USE_BLAS
#include "blas.h"
#endif
#include <cmath>
#include <complex>

// Forward declaration
void computeKernelF(double*,int,double*,int,int,bool,std::complex<double>**);

void computeKernelB(double*,int,double*,int,int,bool,std::complex<double>**);

void computeKernelF(double*, int, double*, int, int, bool, double**);

void computeKernelB(double*, int, double*, int, int, bool, double**);

/*!
    \brief Constructor of the object to compute the cost corresponding to a
        given spectrum.
    \param kn: Array containing the Matsubara frequencies (in)
    \param G_in : Array containing the Matsubara Green's function (in)
    \param sigma : Array containing the fluctuation on the Green's function (in)
    \param nkn_in : Number of Matsubara frequencies (in).
    \param w_in : Array of real frequencies (in)
    \param nw : Number of real frequencies (in).
    \param nw_cut : Number of frequencies after which the spline beahvior change (in)
    \param sym : Is the spectrum symmetric around w=0? (in)
    \param fermions: Are we dealing with fermions (or bosons)? (in)
    \param alpha : Respective weight of the entropy in the cost function (in).
    \param def_in : Array containing the default model used in the entropy (in).
*/
template<class T> energieLibreObj<T>::energieLibreObj(double *kn, T *G_in,
    T *sigma_in, int nkn_in, double *w_in, int nw_in, int nw_cut, bool sym,
    bool fermions, double alpha, double *def_in) {
    weight = NULL;
    def = NULL;
#ifndef USE_BLAS
    sigmai = NULL;
    kerti = NULL;
#endif
    nkn = nkn_in;
    nw = nw_in;
    w = w_in;
    G = new T[nkn];
    for (int i=0; i<nkn; ++i) G[i] = G_in[i];
    sigma = new T[nkn];
    for (int i=0; i<nkn; ++i) sigma[i] = sigma_in[i];

    // Generate the kernel
    ker = new T*[nkn];
    ker[0] = new T[nkn*nw];
    for (int i=1; i<nkn; ++i) ker[i] = ker[i-1] + nw;
    if (fermions) computeKernelF(kn, nkn, w, nw, nw_cut, sym, ker);
    else computeKernelB(kn, nkn, w, nw, nw_cut, sym, ker); 

    // Build the Model and compute the weight
    setModel(def_in);
    setWeight(alpha);

    // Build the required tables to accelerate the computation of the cost
    this->BuildTable(ker, G_in, sigma_in);
}

/*!
    \brief Destructor of the object to compute the cost corresponding to a
        given spectrum.
*/
template<class T> energieLibreObj<T>::~energieLibreObj() {
#ifdef USE_BLAS
    delete [] grad0;
#else
    if (kerti != NULL) {
        for (int i=1; i<nw; ++i) kerti[i] = NULL;
        delete [] kerti[0];
        delete [] kerti;    def = new double[nw];
    }
    for (int i=1; i<nw; ++i) kertr[i] = NULL;
    delete [] kertr[0];
    delete [] kertr;
    if (sigmai != NULL) delete [] sigmai;
    delete [] sigmar;
#endif
    for (int i=1; i<nkn; ++i) ker[i] = NULL;
    delete [] ker[0];
    delete [] ker;
    delete [] sigma;
    delete [] G;
    delete [] Hchi2;
    delete [] weight;
    delete [] def;
}

/*!
    \brief Method that builds some array to accelerate the computation of the
        cost function.
    \param Ker : Array containing the kernel (in).
    \param G_in : Array containing the Matsubara Green's function (in).
    \param sigma : Array containing the fluctuation of the Green's function (in).
    \note This is a specialisation for the complex case.
*/
template<> void
energieLibreObj< std::complex<double> >::BuildTable(std::complex<double> **Ker,
    std::complex<double> *G_in, std::complex<double> *sigma_in)
{
    Hchi2 = new double[nw*nw];
#ifdef USE_BLAS
    int n = 2 * nkn;
    double *scale = new double[n];
    double cst = sqrt(2.);
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (int i=0; i<nkn; ++i) {
        scale[2*i] = cst / sigma[i].real();
        scale[2*i+1] = cst / sigma[i].imag();
    }
    double *gs = new double[n];
    double *M = (double*) &G_in[0];
    int inc = 1;
    char trans = 'N';
    double one = 1.;
    double zero = 0.;
    int m = 0;
    dgbmv_(&trans, &n, &n, &m, &m, &one, M, &inc, scale, &inc, &zero, gs, &inc);
    double *ks = new double[n*nw];
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (int i=0; i<nkn; ++i) {
        int inc2 = 2;
        int inc3 = 2 * nkn;
	double *kptr = (double*) &Ker[i][0];
        dcopy_(&nw, kptr, &inc2, &ks[2*i], &inc3);
        dscal_(&nw, &scale[2*i], &ks[2*i], &inc3);
        dcopy_(&nw, kptr+1, &inc2, &ks[2*i+1], &inc3);
        dscal_(&nw, &scale[2*i+1], &ks[2*i+1], &inc3);
    }
    delete [] scale;
    char tr2 = 'T';
    dgemm_(&tr2, &trans, &nw, &nw, &n, &one, ks, &n, ks, &n, &zero, Hchi2, &nw);
    grad0 = new double[nw];
    dgemv_(&tr2, &n, &nw, &one, ks, &n, gs, &inc, &zero, grad0, &inc);
    delete [] ks;
    chi2_0 = dnrm2_(&n, gs, &inc);
    chi2_0 = 0.5 * chi2_0 * chi2_0;
    delete [] gs;
#else
    sigmar = new double[nkn];
    sigmai = new double[nkn];
    for (int i=0; i<nkn; ++i) {
        sigmar[i] = sigma[i].real();
        sigmai[i] = sigma[i].imag();
    }
    kertr = new double*[nw];
    kertr[0] = new double[nkn*nw];
    kerti = new double*[nw];
    kerti[0] = new double[nkn*nw];
    for (int i=1; i<nw; ++i) {
        kertr[i] = kertr[i-1] + nkn;
        kerti[i] = kerti[i-1] + nkn;
        for (int j=0; j<nkn; ++j) {
            kertr[i][j] = Ker[j][i].real();
            kerti[i][j] = Ker[j][i].imag();
        }
    }
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (int i=0; i<nw; ++i) {
        int k = nw * i;
        for (int j=0; j<nw; ++j, ++k) {
            double sum = 0;
            for (int l=0; l<nkn; ++l) {
                sum += Ker[l][i].real()*Ker[l][j].real() / sigmar[l]*sigmar[l]
                    + Ker[l][i].imag()*Ker[l][j].imag() / sigmai[l]*sigmai[l];
            }
            Hchi2[k]= 2 * sum;
        }
    }
#endif
}

/*!
    \brief Method that builds some array to accelerate the computation of the
        cost function.
    \param Ker : Array containing the kernel (in).
    \param G_in : Array containing the Matsubara Green's function (in).
    \param sigma : Array containing the fluctuation of the Green's function (in).
    \note This is a specialisation for the real case.
*/
template<> void
energieLibreObj<double>::BuildTable(double **Ker, double *G_in,double *sigma_in)
{
    Hchi2 = new double[nw*nw];
#ifdef USE_BLAS
    double *scale = new double[nkn];
    double cst = sqrt(2.);
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (int i=0; i<nkn; ++i) {
        scale[i] = cst / sigma[i];
    }
    double *gs = new double[nkn];
    int inc = 1;
    char trans = 'N';
    double one = 1.;
    double zero = 0.;
    int m = 0;
    dgbmv_(&trans,&nkn,&nkn,&m,&m,&one,G_in,&inc,scale,&inc,&zero,gs,&inc);
    double *ks = new double[nkn*nw];
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (int i=0; i<nkn; ++i) {
        dcopy_(&nw, Ker[i], &inc, &ks[i], &nkn);
        dscal_(&nw, &scale[i], &ks[i], &nkn);
    }
    delete [] scale;
    char tr2 = 'T';
    dgemm_(&tr2,&trans,&nw,&nw,&nkn,&one,ks,&nkn,ks,&nkn,&zero,Hchi2,&nw);
    grad0 = new double[nw];
    dgemv_(&tr2, &nkn, &nw, &one, ks, &nkn, gs, &inc, &zero, grad0, &inc);
    delete [] ks;
    chi2_0 = dnrm2_(&nkn, gs, &inc);
    chi2_0 = 0.5 * chi2_0 * chi2_0;
    delete [] gs;
#else
    sigmar = new double[nkn];
    sigmai = NULL;
    for (int i=0; i<nkn; ++i) {
        sigmar[i] = sigma[i];
    }
    kertr = new double*[nw];
    kertr[0] = new double[nkn*nw];
    kerti = NULL;
    for (int i=1; i<nw; ++i) {
        kertr[i] = kertr[i-1] + nkn;
        for (int j=0; j<nkn; ++j) kertr[i][j] = Ker[j][i];
    }
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (int i=0; i<nw; ++i) {
        int k = nw * i;
        for (int j=0; j<nw; ++j, ++k) {
            double sum = 0;
            for (int l=0; l<nkn; ++l) {
                sum += Ker[l][i]*Ker[l][j] / sigmar[l]*sigmar[l];
            }
            Hchi2[k]= 2 * sum;
        }
    }
#endif
}

/*!
    \brief Method that returns a structure containing the extra arguments of
        the cost function.
    \return an argCostType structure. It contains a pointer to the current
        object and the class of template used.
    \note This is a specialisation for the complex case.
*/
template<> argCostType energieLibreObj<std::complex<double> >::GetArgumentCost()
{
    argCostType argCost;
    argCost.data_type = 'c';
    argCost.obj_ptr = (long*) this;
    return argCost;
}

/*!
    \brief Method that returns a structure containing the extra arguments of
        the cost function.
    \return an argCostType structure. It contains a pointer to the current
        object and the class of template used.
    \note This is a specialisation for the real case.
*/
template<> argCostType energieLibreObj<double>::GetArgumentCost()
{
    argCostType argCost;
    argCost.data_type = 'd';
    argCost.obj_ptr = (long*) this;
    return argCost;
}

/*!
    \brief Generate the model for the entropy
    \param def_in : Model to use (will be scaled by a normalisation factor).
*/
template<> void energieLibreObj<double>::setModel(double *def_in)
{
    if (def == NULL) def = new double[nw];
    if (def_in == NULL) {
        double cst = exp(1.0) / nw;
        for (int i=0; i<nw; ++i) def[i] = cst;
    } else {
        double sum = 0.;
        for (int i=0; i<nw; ++i) sum += ker[0][i] * def_in[i];
        double cst = exp(1.0) * G[0] / sum;
        for (int i=0; i<nw; ++i) def[i] = def_in[i] * cst;
        for (int i=0; i<nw; ++i) def_in[i] = def[i];
    }
}

/*!
    \brief Generate the model for the entropy
    \param def_in : Model to use (will be scaled by a normalisation factor).
*/
template<> void energieLibreObj<std::complex<double> >::setModel(double *def_in)
{
    if (def == NULL) def = new double[nw];
    if (def_in == NULL) {
        double cst = exp(1.0) / nw;
        for (int i=0; i<nw; ++i) def[i] = cst;
    } else {
        double sum = 0.;
        for (int i=0; i<nw; ++i) sum += ker[0][i].real() * def_in[i];
        double cst = exp(1.0) * G[0].real() / sum;
        for (int i=0; i<nw; ++i) def[i] = def_in[i] * cst;
    }
}

/*!
    \brief Compute the weight of each frequency interval for the entropy
    \param alpha : Relative weight of the entropy.
*/
template<class T> void energieLibreObj<T>::setWeight(double alpha)
{
    if (weight == NULL) weight = new double[nw];
    weight[0] = alpha * 0.5 * (w[1] - w[0]);
    for (int i=1; i<nw-1; ++i) {
        weight[i] = alpha * 0.5 * (w[i+1] - w[i-1]);
    }
    weight[nw-1] = alpha * 0.5 * (w[nw-1] - w[nw-2]);
}

#ifdef USE_BLAS
/*!
    \brief This method compute the cost corresponding to a given spectrum and
        returns its gradient and Hessian.
    \param x : Array containing the spectrum (in).
    \param grad : Array containing the gradient of the cost (out; allocated on input)
    \param H : Array containing the Hessian of the cost (out; allocated on input)
    \return the value of the cost function.
    \note This is the version for when the user have Blas.
*/
template<class T>
double energieLibreObj<T>::ComputeCost(double *x, double *grad, double *H)
{
    double val;
    // Put the likelihood hessian in the hessian matrix
    int inc = 1;
    int m = nw * nw;
    dcopy_(&m, Hchi2, &inc, H, &inc);

    // Add the hessian of the entropy (diagonal contribution only)
    int inc2 = nw+1;
    double one = 1.;
    double *buf = new double[nw];
    for (int i=0; i<nw; ++i) buf[i] = weight[i] / x[i];
    daxpy_(&nw, &one, buf, &inc, H, &inc2);
    delete [] buf;

    // Compute the likelihood function and its gradient
    char trans = 'T';
    double zero = 0.;
    dgemv_(&trans, &nw, &nw, &one, Hchi2, &nw, x, &inc, &zero, grad, &inc);
    val = 0.5 * ddot_(&nw, grad, &inc, x, &inc) + chi2_0
        - ddot_(&nw, grad0, &inc, x, &inc);
    double mone = -1.;
    daxpy_(&nw, &mone, grad0, &inc, grad, &inc);

    // Compute the entropy function and its gradient
    #ifdef _OPENMP
    #pragma omp parallel
    #endif
    {
        double lval = 0.;
        #ifdef _OPENMP
        #pragma omp for
        #endif
        for (int i=0; i<nw; ++i) {
            double lnx = weight[i] * log(x[i] / def[i]);
            grad[i] += weight[i] + lnx;
            lval += lnx * x[i];
        }
        #ifdef _OPENMP
        #pragma omp critical
        #endif
        {
            val += lval;
        }
    }
    return val;
}

template<> double energieLibreObj<double>::ComputeCost(double *x, double *grad, double *H)
{
    char trans = 'T';
    double zero = 0.;
    double one = 1.0;
    int inc = 1;
    // Put the likelihood hessian in the hessian matrix
    int m = nw * nw;
    dcopy_(&m, Hchi2, &inc, H, &inc);
    // Add the hessian of the entropy (diagonal contribution only)
    int inc2 = nw+1;
    double *buf = new double[nw];
    for (int i=0; i<nw; ++i) buf[i] = weight[i] / x[i];
    daxpy_(&nw, &one, buf, &inc, H, &inc2);
    delete [] buf;

    buf = new double[nkn];
    dgemv_(&trans, &nw, &nkn, &one, ker[0], &nw, x, &inc, &zero, buf, &inc);
    double cost = 0.;
#ifdef _OPENMP
#pragma omp parallel
#endif
    {
        double sum = 0.;
#ifdef _OPENMP
#pragma omp for
#endif
        for (int i=0; i<nkn; ++i) {
            double val = (buf[i] - G[i]) / sigma[i];
            sum += val * val;
            buf[i] = 2 * val / sigma[i];
        }
#ifdef _OPENMP
#pragma omp critical
#endif
        {
            cost += sum;
        }
    }
    trans = 'N';
    dgemv_(&trans, &nw, &nkn, &one, ker[0], &nw, buf, &inc, &zero, grad, &inc);
    delete [] buf;
    #ifdef _OPENMP
    #pragma omp parallel
    #endif
    {
        double sum = 0.;
        #ifdef _OPENMP
        #pragma omp for
        #endif
        for (int i=0; i<nw; ++i) {
            double lnx = weight[i] * log(x[i] / def[i]);
            grad[i] += weight[i] + lnx;
            sum += lnx * x[i];
        }
        #ifdef _OPENMP
        #pragma omp critical
        #endif
        {
            cost += sum;
        }
    }
    return cost;
}

/*!
    \brief This method compute the likelihood function.
    \param x : Array containing the spectrum (in).
    \return the value of the likelihood function.
    \note This is the version for when the user have Blas.
*/
template<class T> double energieLibreObj<T>::ComputeLikelihood(double *x)
{
    char trans = 'T';
    double zero = 0.;
    double one = 1.0;
    int inc = 1;
    double *buf = new double[nw];
    dgemv_(&trans, &nw, &nw, &one, Hchi2, &nw, x, &inc, &zero, buf, &inc);
    double chi2 = 0.5 * ddot_(&nw, buf, &inc, x, &inc) + chi2_0
        - ddot_(&nw, grad0, &inc, x, &inc);
    delete [] buf;
    return chi2;
}

/*!
    \brief Compute the relative error on the Green's function
    \param x : Array containing the spectrum (in).
    \return A scalar double containing the relative error.
    \note This is the version for when the user have Blas.
*/
template<> double energieLibreObj<double>::ComputeRelativeError(double *x)
{
    char trans = 'T';
    double zero = 0.;
    double one = 1.0;
    int inc = 1;
    double *buf = new double[nkn];
    dgemv_(&trans, &nw, &nkn, &one, ker[0], &nw, x, &inc, &zero, buf, &inc);
    double error = 0.;
#ifdef _OPENMP
#pragma omp parallel
#endif
    {
        double sum = 0.;
#ifdef _OPENMP
#pragma omp for
#endif
        for (int i=0; i<nkn; ++i) {
            sum += fabs(1.0 - buf[i] / G[i]);
        }
#ifdef _OPENMP
#pragma omp critical
#endif
        {
            error += sum;
        }
    }
    delete [] buf;
    return error;
}

#else

/*!
    \brief This method compute the cost corresponding to a given spectrum and
        returns its gradient and Hessian.
    \param x : Array containing the spectrum (in).
    \param grad : Array containing the gradient of the cost (out; allocated on input)
    \param H : Array containing the Hessian of the cost (out; allocated on input)
    \return the value of the cost function.
    \note This is a specialization for the complex case (without blas).
*/
template<> double energieLibreObj<std::complex<double> >::ComputeCost(double *x,
    double *grad, double *H)
{
    double val;
    double *bufr = new double[nkn];
    double *bufi = new double[nkn];
    val = 0.;
    #ifdef _OPENMP
    #pragma omp parallel
    #endif
    {
        double lval = 0;
        // Compute the chi2 and put each contribution in a buffer.
        #ifdef _OPENMP
        #pragma omp parallel for
        #endif
        for (int i=0; i<nkn; ++i) {
            std::complex<double> gt = 0;
            for (int j=0; j<nw; ++j) gt += ker[i][j] * x[j];
            bufr[i] = (G[i] - gt).real() / sigmar[i];
            bufi[i] = (G[i] - gt).imag() / sigmai[i];
            lval += bufr[i] * bufr[i] + bufi[i] * bufi[i];
        }

        #ifdef _OPENMP
        #pragma omp barrier
        #endif

        // Compute the hessian and the entropy contribution to the gradient and
        // to the cost
        #ifdef _OPENMP
        #pragma omp for
        #endif
        for (int i=0; i<nw; ++i) {
            int k = i * nw;
            for (int j=0; j<i; ++j, ++k) H[k] = Hchi2[k];
            H[k] = Hchi2[k] + weight[i] / x[i];
            ++k;
            for (int j=i+1; j<nw; ++j, ++k) H[k] = Hchi2[k];
            double lnx = weight[i] * log(x[i] / def[i]);
            double sum = 0;
            for (int j=0; j<nkn; ++j)
                sum += kertr[i][j] * bufr[j] / sigmar[j]
                    +kerti[i][j] * bufi[j] / sigmai[j];
            grad[i] = weight[i] + lnx - 2 * sum;
            lval += lnx * x[i];
        }

        // Assemble the result
        #ifdef _OPENMP
        #pragma omp critical
        #endif
        {
            val += lval;
        }
    }
    return val;
}

/*!
    \brief This method compute the likelihood function.
    \param x : Array containing the spectrum (in).
    \return the value of the likelihood function.
    \note This is a specialization for the complex case (without blas).
*/
template<>
double energieLibreObj<std::complex<double> >::ComputeLikelihood(double *x)
{
    double val = 0.;
    #ifdef _OPENMP
    #pragma omp parallel
    #endif
    {
        double lval = 0;
        // Compute the chi2 and put each contribution in a buffer.
        #ifdef _OPENMP
        #pragma omp parallel for
        #endif
        for (int i=0; i<nkn; ++i) {
            std::complex<double> gt = 0;
            for (int j=0; j<nw; ++j) gt += ker[i][j] * x[j];
            double bufr = (G[i] - gt).real() / sigmar[i];
            double bufi = (G[i] - gt).imag() / sigmai[i];
            lval += bufr * bufr + bufi * bufi;
        }
        #ifdef _OPENMP
        #pragma omp critical
        #endif
        {
            val += lval;
        }
    }
    return val;
}

/*!
    \brief This method compute the cost corresponding to a given spectrum and
        returns its gradient and Hessian.
    \param x : Array containing the spectrum (in).
    \param grad : Array containing the gradient of the cost (out; allocated on input)
    \param H : Array containing the Hessian of the cost (out; allocated on input)
    \return the value of the cost function.
    \note This is a specialization for the real case (without blas).
*/
template<> double energieLibreObj<double>::ComputeCost(double *x, double *grad,
    double *H)
{
    double val;
    double *bufr = new double[nkn];
    val = 0.;
    #ifdef _OPENMP
    #pragma omp parallel
    #endif
    {
        double lval = 0;
        // Compute the chi2 and put each contribution in a buffer.
        #ifdef _OPENMP
        #pragma omp parallel for
        #endif
        for (int i=0; i<nkn; ++i) {
            double gt = 0;
            for (int j=0; j<nw; ++j) gt += ker[i][j] * x[j];
            bufr[i] = (G[i] - gt) / sigmar[i];
            lval += bufr[i] * bufr[i];
        }

        #ifdef _OPENMP
        #pragma omp barrier
        #endif

        // Compute the hessian and the entropy contribution to the gradient and
        // to the cost
        #ifdef _OPENMP
        #pragma omp for
        #endif
        for (int i=0; i<nw; ++i) {
            int k = i * nw;
            for (int j=0; j<i; ++j, ++k) H[k] = Hchi2[k];
            H[k] = Hchi2[k] + weight[i] / x[i];
            ++k;
            for (int j=i+1; j<nw; ++j, ++k) H[k] = Hchi2[k];
            double lnx = weight[i] * log(x[i] / def[i]);
            double sum = 0;
            for (int j=0; j<nkn; ++j) sum += kertr[i][j] * bufr[j] / sigmar[j];
            grad[i] = weight[i] + lnx - 2 * sum;
            lval += lnx * x[i];
        }

        // Assemble the result
        #ifdef _OPENMP
        #pragma omp critical
        #endif
        {
            val += lval;
        }
    }
    return val;
}

/*!
    \brief This method compute the likelihood function.
    \param x : Array containing the spectrum (in).
    \return the value of the likelihood function.
    \note This is a specialization for the real case (without blas).
*/
template<> double energieLibreObj<double>::ComputeLikelihood(double *x)
{
    double val = 0.;
    #ifdef _OPENMP
    #pragma omp parallel
    #endif
    {
        double lval = 0;
        // Compute the chi2 and put each contribution in a buffer.
        #ifdef _OPENMP
        #pragma omp parallel for
        #endif
        for (int i=0; i<nkn; ++i) {
            double gt = 0;
            for (int j=0; j<nw; ++j) gt += ker[i][j] * x[j];
            double bufr = (G[i] - gt) / sigmar[i];
            lval += bufr * bufr;
        }
        #ifdef _OPENMP
        #pragma omp critical
        #endif
        {
            val += lval;
        }
    }
    return val;
}

/*!
    \brief Compute the relative error on the Green's function
    \param x : Array containing the spectrum (in).
    \return A scalar double containing the relative error.
    \note This is a specialization for the real case (without blas).
*/
template<> double energieLibreObj<double>::ComputeRelativeError(double *x)
{
    double error = 0.;
#ifdef _OPENMP
#pragma omp parallel
#endif
    {
        double sum = 0.;
#ifdef _OPENMP
#pragma omp for
#endif
        for (int i=0; i<nkn; ++i) {
            double buf = 0.;
            for (int j=0; j<nw; ++j) {
                buf += ker[i][j] * x[j];
            }
            sum += fabs(1.0 - buf / G[i]);
        }
#ifdef _OPENMP
#pragma omp critical
#endif
        {
            error += sum;
        }
    }
    return error;
}
#endif

/*!
    \brief Compute the relative error on the Green's function
    \param x : Array containing the spectrum (in).
    \return A scalar double containing the relative error.
    \note This is a specialization for the complex case.
*/
template<> double
energieLibreObj<std::complex<double> >::ComputeRelativeError(double *x)
{
    double error = 0.;
#ifdef _OPENMP
#pragma omp parallel
#endif
    {
        double sum = 0.;
#ifdef _OPENMP
#pragma omp for
#endif
        for (int i=0; i<nkn; ++i) {
            std::complex<double> buf = std::complex<double>(0., 0.);
            for (int j=0; j<nw; ++j) {
                buf += ker[i][j] * x[j];
            }
            sum += fabs(1.0 - buf.real() / G[i].real());
            sum += fabs(1.0 - buf.imag() / G[i].imag());
        }
#ifdef _OPENMP
#pragma omp critical
#endif
        {
            error += sum;
        }
    }
    return error;
}

/*!
    \brief This method initialise the spectrum, the cost function, its gradient
        and its Hessian.
    \param x : Array containing the spectrum (in/out).
    \param grad : Array containing the gradient of the cost (out; allocated on input)
    \param H : Array containing the Hessian of the cost (out; allocated on input)
    \param isXInitialized : If X is not initialized, initialize it. (in)
    \return the value of the cost function.
*/
template<class T> double energieLibreObj<T>::initiliazeSpect(double *x,
    double *grad, double *H, bool isXInitialized)
{
    if (! isXInitialized) {
        for (int i=0; i<nw; ++i) x[i] = def[i];
    }
    return ComputeCost(x, grad, H);
}

/*!
    \brief This method returns the extremum for the variables.
    \param l : will be allocated and contain the lowest allowed values (out).
    \param u : will be allocated and contain the highest allowed values (out).
    \param n : contain the size of the spectrum (out).
*/
template<class T>
void energieLibreObj<T>::GetLimits(double **l, double **u, int *n) {
    double amin = 1.e-200;
    double *ll = new double[nw];
    double *ul = new double[nw];
    double amax = 20. / (w[2] - w[0]);
    for (int i=0; i<nw-1; ++i) {
        ll[i] = amin;
        ul[i] = amax;
        amax = 20. / (w[i+2] - w[i]);
    }
    ll[nw-1] = amin;
    ul[nw-1] = 0.5 * ul[nw-2];
    *n = nw;
    *l = ll;
    *u = ul;
    ll = NULL;
    ul = NULL;
}

/*!
    \brief This function allows to have a pointer to the cost function
        without having to define a pointer to the method (two different
        things in C++).
    \param x : Pointer to the current spectrum (in)
    \param g : Pointer to the current gradient of the cost function (in).
    \param H : Pointer to the current Hessian of the cost function (in).
    \param arg : Structure containing a ptr to the energieLibreObj and a
        string defining the type of template. (in)
    \return the Cost function value for the spectrum x.
*/
double energieLibre(double *x, double *g, double *H, void *arg) {
    argCostType *argCost = (argCostType*) arg;
    double cost;
    if (argCost->data_type == 'c') {
        energieLibreObj<std::complex<double> > *obj
            = (energieLibreObj<std::complex<double> >*) argCost->obj_ptr;
        cost = obj->ComputeCost(x, g, H);
    } else {
        energieLibreObj<double> *obj
            = (energieLibreObj<double>*) argCost->obj_ptr;
        cost = obj->ComputeCost(x, g, H);
    }
    return cost;
}

