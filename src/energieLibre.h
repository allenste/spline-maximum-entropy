/*!
    \file energieLibre.h
    \author Steve Allen, CCS, Universite de Sherbrooke
    \date September 2011

    \brief This file contains a class that enables to build an algorithm for
        the optimisation of a given function under constraint.
*/
/*
 * Copyright (C) 2011 Steve Allen
 *
 * This file is part of SplineMaxEnt.
 * 
 * SplineMaxEnt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SplineMaxEnt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SplineMaxEnt.  If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef ENERGIELIBRE_H_
#define ENERGIELIBRE_H_

#include <complex>

/*!
    \brief Structure that allows to pass extra arguments to the function we
    want to minimise
*/
typedef struct {
    char data_type;
    long *obj_ptr;
} argCostType;

// C Interface to the cost function of a energieLibreObj intstantiation
double energieLibre(double*, double*, double*, void*);

/*!
 * \brief Class that create object to compute the cost function.
*/
template<class T> class energieLibreObj {
    public:
    energieLibreObj(double*, T*, T*, int, double*, int, int, bool, bool,
        double, double*);

    ~energieLibreObj();

    double ComputeCost(double*, double*, double*);

    double initiliazeSpect(double*, double*, double*, bool);

    void BuildTable(T**, T*, T*);

    argCostType GetArgumentCost();

    void GetLimits(double**, double**, int*);

    void setWeight(double);

    void setModel(double*);

    inline int getNumberFreq() { return nw; }

    double ComputeLikelihood(double *A);

    double ComputeRelativeError(double *A);

    private:
        /*! Number of Matsubara frequencies */
        int nkn;

        /*! Number of real frequencies */
        int nw;

        /*! Weight of each interval of frequencies in the entropy */
        double *weight;

        /*! Default model used in the entropy */
        double *def;

        /*! Least Square function contribution to the Hessian */
        double *Hchi2;

        /*! Kernel linking the spectrum to the Green's function */
        T **ker;

        /*! Array containing the list of frequencies */
        double *w;

        /*! Pointer to the Matsubara Green's function we want to fit */
        T *G;

        /*! Pointer to the precision on the Green's function */
        T *sigma;

        #ifdef USE_BLAS
            /*! Contribution to the gradient that is independent of A */
            double *grad0;

            /*! Contribution to the least square function independant of A */
            double chi2_0;
        #else
            /*! Size of the flucutuation on the real part of G */
            double *sigmar;

            /*! Size of the fluctuation on the imaginary part of G */
            double *sigmai;

            /*! Real part of the transpose of the kernel */
            double **kertr;

            /*! Imaginary part of the transpose of the kernel */
            double **kerti;
        #endif
};

#endif /* ifndef ENERGIELIBRE_H_ */

