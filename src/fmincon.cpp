/*!
    \file fmincon.cpp
    \author Steve Allen, CCS, Universite de Sherbrooke
    \date August 2011

    \brief This file contains a class that enables to build an algorithm for
        the optimisation of a given function under constraint.
*/
/*
 * Copyright (C) 2011 Steve Allen
 *
 * This file is part of SplineMaxEnt.
 * 
 * SplineMaxEnt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SplineMaxEnt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SplineMaxEnt.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "fmincon.h"
#include <fstream>

/*!
    \brief Contructor for the optimisation object
    \param n : number of variables (in)
    \param l_in : array containing the lowest value allowed for each variable (in)
    \param u_in : array containing the highest value allowed for each variable (in)
    \param A : Pointer to the current values of the variables (in)
    \param cost : Current value of the cost function (in)
    \param grad_in : Pointer to the current gradient of the cost function (in).
    \param H_in : Pointer to the current Hessian of the cost function (in).
*/
Optimize::Optimize(int n, double *l_in, double *u_in, double *A, double cost,
    double *grad_in, double *H_in)
{
    SetDefaultValues();
    nx = n;
    l = l_in;
    u = u_in;
    this->SetInitialPoint(A, cost, grad_in, H_in);
#if VERBOSE >= 1
    PrintDescription();
#endif
}

/*!
    \brief Contructor for the optimisation object
    \param n : number of variables (in)
    \param l_in : array containing the lowest value allowed for each variable (in)
    \param u_in : array containing the highest value allowed for each variable (in)
*/
Optimize::Optimize(int n, double *l_in, double *u_in) {
    SetDefaultValues();
    nx = n;
    l = l_in;
    u = u_in;
#if VERBOSE >= 1
    PrintDescription();
#endif
}

/*!
    \brief Destructor of the object (remove the pointers)
*/
Optimize::~Optimize() {
    delete [] l;
    delete [] u;
    delete [] grad;
    delete [] H;
    SetDefaultValues();
}

/*!
    \brief Driver that launch the minimisation of a function
    \param fct: Pointer to the function we want to minimise (in)
    \param arg : Pointer to the additional argument of the function (in).
    \note This function modifies the values in the array pointed by x, grad, H.
*/
void Optimize::Driver(double (*fct)(double*,double*,double*,void*), void *arg)
{
    switch (algo) {
    case TRUSTREGIONREFLECTIVE :
        result = sfminbx(result.mincost, x, grad, H, nx, l, u, fct, arg);
    }
}

/*!
    \brief Function that set the default values of the attribute of the class.
*/
void Optimize::SetDefaultValues() {
    algo = TRUSTREGIONREFLECTIVE;
    nx = 0;
    l = NULL;
    u = NULL;
    x = NULL;
    grad = NULL;
    H = NULL;
    result.mincost = 0.;
    result.finished = 0;
    result.iter = 0;
    result.numFunEvals = 0;
    result.numAccepChange = 0;
    result.diff = 0.;
    result.gnrm = 0.;
    result.nrmsx = 0.;
}

/*!
    \brief This function set the initial value of the variable and the cost function
    \param A : Pointer to the current variable (in)
    \param cost : Pointer to the current value of the cost function (in)
    \param g : Pointer to the current gradient of the cost function (in)
    \param H_in : Pointer to the current Hessian of the cost function (in)
*/
void Optimize::SetInitialPoint(double *A, double cost, double *g, double *H_in)
{
    x = A;
    if (grad != NULL && grad != g) delete [] grad;
    grad = g;
    if (H != NULL && H != H_in) delete [] H;
    H = H_in;
    result.mincost = cost;
}

/*!
    \brief Print a description of the algorithm used.
    \param out: Pointer to the ouput stream (inout).
*/
void Optimize::PrintDescription(std::ostream *out) const {
    *out << "Optmize Program version 1.0\n";
    *out << "By Steve Allen, CCS, Universite de Sherbrooke\n";
    *out << "Using algorithm : \n";
    if (algo == TRUSTREGIONREFLECTIVE) {
        *out << "Trust Region Reflective based on Matlab sfminbx function.\n";
    }
    *out << "It is part of SplineMaxEnt that does analytical continutation ";
    *out << "of a Green function.\n";
    *out << "Adaptation of Dominic Bergeron Matlab program.\n";
    *out << "\n";
}

/*!
    \brief Print the statistics of the optmisation process.
    \param out: Pointer to the ouput stream (inout).
*/
void Optimize::PrintStatistics(std::ostream *out) const {
    *out << "Minimum cost reach " << result.mincost << "\n";
    *out << "Number of iterations " << result.iter << "\n";
    *out << "Number of function calls " << result.numFunEvals << "\n";
    *out << "Number of accepted move " << result.numAccepChange << "\n";
    *out << "Relative function value change " << result.diff << "\n";
    *out << "Norm of the gradient " << result.gnrm << "\n";
    *out << "Norm of the last step " << result.nrmsx << "\n";
    switch (result.finished) {
    case 1 :
        *out << "Optimization terminated: relative function value ";
        *out << "changing by less than the required tolerace\n";
        break;
    case 2 :
        *out << "Optimization terminated: norm of the current step is ";
        *out << "less than the required tolerance.\n";
        break;
    case 3 :
        *out << "Optimization terminated: first-order optimality less ";
        *out << " than the required tolerance\n";
        break;
    case 4 :
        *out << "Maximum number of function evaluations exceeded;\n";
        break;
    case 5 :
        *out << "Maximum number of iterations exceeded;\n";
    case 6 :
        *out << "No move accepted for many tries;\n";
    }
}

