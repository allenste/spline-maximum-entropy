/*!
    \file pcgr.cpp
    \author Steve Allen, CCS, Universite de Sherbrooke
    \date August 2011

    \brief Preconditioned conjugate gradients. Based on Matlab pcgr.
*/
/*
 * Copyright (C) 2011 Steve Allen
 *
 * This file is part of SplineMaxEnt.
 * 
 * SplineMaxEnt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SplineMaxEnt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SplineMaxEnt.  If not, see <http://www.gnu.org/licenses/>.
**/
#include <cmath>
#include <iostream>
#include "util.h"
#ifdef USE_BLAS
#include "blas.h"
#endif

/*!
    \brief apply a preconditioned conjugate gradient procedure to the
        quadratic q(p) = .5p'Mp + g'p.
    \param WW = DM*H*DM + DG.
    \param R is the cholesky factor of the preconditioner (transpose) of M.
    \param g is the gradient
    \param p is the computed direction (out).
    \param npcgr is the cumulative number of iterations in pcgr (out).
    \return posdef = 1 implies only positive curvature (in M) has been
        detected; posdef = 0 implies p is a direction of negative curvature
        (for M).
*/
int pcgr(double *g, double *R, double **WW, int n, double *p, int *npcgr) {
#ifdef USE_BLAS
    const double tol = 0.1;
#else
    const double tol = 0.01;  // Note it is the square of the matlab version
                              // because we work with the square norm
#endif
    // bound on the number of permitted CG-iterations
    int kmax = (n > 2 ? n / 2 : 1);
    int posdef = 1;
    double *r = new double[n];
#ifdef USE_BLAS
    for (int i=0; i<n; ++i) p[i] = 0;
    int inc = 1;
    dcopy_(&n, g, &inc, r, &inc);
    double minus_one = -1.0;
    dscal_(&n, &minus_one, r, &inc);
#else
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (int i=0; i<n; ++i) {
        r[i] = -1 * g[i];
        p[i] = 0.;
    }
#endif

    // Precondition
    double *z = new double[n];
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (int i=0; i<n; ++i) {
        z[i] = r[i] / (R[i] * R[i]);
    }
    double *d = new double[n];
#ifdef USE_BLAS
    double stoptol = tol * dnrm2_(&n, z, &inc);
    double inner1 = ddot_(&n, r, &inc, z, &inc);
    dcopy_(&n, z, &inc, d, &inc);
#else
    double stoptol = 0;
    for (int i=0; i<n; ++i) stoptol += z[i] * z[i];
    stoptol *= tol;
    double inner1 = 0;
    for (int i=0; i<n; ++i) inner1 += r[i] * z[i];
    for (int i=0; i<n; ++i) d[i] = z[i];
#endif
    double *w = new double[n];

    // Primary Loop
    int k = 0;
    for ( ; k < kmax; ++k) {
        #ifdef USE_BLAS
        char tr = 't';
        double one = 1.0;
        double zero = 0.;
        dgemv_(&tr, &n, &n, &one, WW[0], &n, d, &inc, &zero, w, &inc);
        double denom = ddot_(&n, d, &inc, w, &inc);
        #else
        double denom = 0;
        for (int i=0; i<n; ++i) {
            double sum = 0;
            for (int j=0; j<n; ++j) sum += WW[i][j] * d[j];
            w[i] = sum;
            denom += d[i] * w[i];
        }
        #endif

        if (denom <= 0.) {
            normalize(d, n, p);
            posdef = 0;
            ++k;
            break;
        } else {
            double alpha = inner1 / denom;
            #ifdef USE_BLAS
            daxpy_(&n, &alpha, d, &inc, p, &inc);
            double da = -1 * alpha;
            daxpy_(&n, &da, w, &inc, r, &inc);
            #else
            #ifdef _OPENMP
            #pragma omp parallel for
            #endif
            for (int i=0; i<n; ++i) {
                p[i] += alpha * d[i];
                r[i] -= alpha * w[i];
            }
            #endif
        }
        #ifdef _OPENMP
        #pragma omp parallel for
        #endif
        for (int i=0; i<n; ++i) {
            z[i] = r[i] / (R[i] * R[i]);
        }

        #ifdef USE_BLAS
        double znrm = dnrm2_(&n, z, &inc);
        if (znrm <= stoptol) {
            ++k;
            break;
        }
        double inner2 = inner1;
        inner1 = ddot_(&n, r, &inc, z, &inc);
        double alpha = inner1 / inner2;
        dscal_(&n, &alpha, d, &inc);
        daxpy_(&n, &one, z, &inc, d, &inc);

        #else
        double znrm = 0;
        for (int i=0; i<n; ++i) znrm += z[i] * z[i];
        if (znrm <= stoptol) {
            ++k;
            break;
        }
        double inner2 = inner1;
        inner1 = 0;
        for (int i=0; i<n; ++i) inner1 += r[i] * z[i];
        double alpha = inner1 / inner2;
        for (int i=0; i<n; ++i) d[i] = z[i] + alpha * d[i];
        #endif
    }
    *npcgr += k;

    // Clean memory
    delete [] w;
    delete [] d;
    delete [] z;
    delete [] r;
    return posdef;
}

