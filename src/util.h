/*!
    \file util.h
    \author Steve Allen, CCS, Universite de Sherbrooke
    \date August 2011

    \brief This file declares some useful mathematical functions.
*/
/*
 * Copyright (C) 2011 Steve Allen
 *
 * This file is part of SplineMaxEnt.
 * 
 * SplineMaxEnt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SplineMaxEnt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SplineMaxEnt.  If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef UTIL_H_
#define UTIL_H_

void normalize(double*, int);

void normalize(double*, int, double*);

void orthogonalize(double*, double*, int);

void matrixMult(double**, double**, double**, int, int, int);

void matrixMultAdd(double**, double**, double**, int, int, int);

void eigen(double*, int, double*, double*);

void inverseMatrix(double**, int);

#endif

