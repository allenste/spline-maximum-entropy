/*!
    \file trdog.cpp
    \author Steve Allen, CCS, Universite de Sherbrooke
    \date August 2011

    \brief This file defines a function to compute the reflected (2-D) trust
    region trial step (box constraints). Based on Matlab Trdog.
*/
/*
 * Copyright (C) 2011 Steve Allen
 *
 * This file is part of SplineMaxEnt.
 * 
 * SplineMaxEnt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SplineMaxEnt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SplineMaxEnt.  If not, see <http://www.gnu.org/licenses/>.
**/
#include <limits>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include "util.h"
#ifdef USE_BLAS
#include "blas.h"
#endif

void trust(double*, double**, double, int, double*);

int pcgr(double*, double*, double**, int, double*, int*);

/*!
    \brief Build the matrix and the right-hand side of the Cholesky problem.
    \param Z : matrix (in)
    \param WW : matrix (in)
    \param grad : gradient (in)
    \param m : number of columns of Z and size of grad (in)
    \param n : Number of rows of Z (in)
    \param MM : Cholesky matrix (out)
    \param rhs : Right-hand side of the Cholesky problem (out).
*/
void ReduceToSubspace(double **Z, double **WW, double *grad, int m, int n,
    double **MM, double *rhs) {
#ifdef USE_BLAS
    char tr = 't';
    double one = 1.0;
    double zero = 0.0;
    int inc = 1;
    dgemv_(&tr, &m, &n, &one, Z[0], &m, grad, &inc, &zero, rhs, &inc);
    double *buf = new double[n * m];
    char ntr = 'n';
    dgemm_(&ntr,&ntr,&m,&n,&m,&one,WW[0],&m,Z[0],&m,&zero,buf,&m);
    dgemm_(&tr,&ntr,&n,&n,&m,&one,Z[0],&m,buf,&m,&zero,MM[0],&n);
    delete [] buf;
#else
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (int i=0; i<n; ++i) {
        double s1 = 0;
        for (int k=0; k<m; ++k) s1 += Z[i][k] * grad[k];
        rhs[i] = s1;
        for (int j=0; j<n; ++j) {
            s1 = 0;
            for (int k=0; k<m; ++k) {
                double s2 = 0;
                for (int l=0; l<m; ++l) s2 += WW[k][l] * Z[j][l];
                s1 += s2 * Z[i][k];
            }
            MM[i][j] = s1;
        }
    }
#endif
}

/*!
    \brief 1D quadratic zero finder for trust region step. ay^2 + by + c
        with a = x*x, b = 2*ss*x, c = ss*ss - delta*delta
    \param x: point where to evaluate; on output contains x*tau
    \param ss: vector to evaluate the parameters a, b, c
    \param delta: step size
    \param n: number of element in x and ss.
    \return tau = min(1, step-to-zero)
*/
double quad1d(double *x, double *ss, double delta, int n) {
#ifdef USE_BLAS
    int inc = 1;
    double a = ddot_(&n, x, &inc, x, &inc);
    double b = 2 * ddot_(&n, ss, &inc, x, &inc);
    double c = ddot_(&n, ss, &inc, ss, &inc) - delta * delta;
#else
    double a = 0;
    double b = 0;
    double c = -1 * delta*delta;
    for (int i=0; i<n; ++i) {
        a += x[i]*x[i];
        b += 2*ss[i]*x[i];
        c += ss[i]*ss[i];
    }
#endif
    double numer = sqrt(b*b - 4*a*c);
    numer = (b > 0. ? -1*(numer + b) : numer - b);
    double r1 = numer * 0.5 / a;
    double r2 = 2 * c / numer;
    double tau = (r1 > r2 ? r1 : r2);
    tau = (tau > 1.0 ? 1.0 : tau);
    for (int i=0; i<n; ++i ) x[i] *= tau;
    return tau;
}

/*!
   \brief sparse Cholesky factor (transpose of a (usually) banded
       preconditioner of square matrix WW = DM*H*DM + DG
       where DM and DG are non-negative sparse diagonal matrices.
   \param WW: matrix to compute the Cholesky factor of.
   \param n: size of the matrix
   \param R: Cholesky factor (output)
*/
void hprecon(double **WW, int n, double *R)
{
    const double epsi = 1.e-4;
    const double epsi2 = epsi*epsi;
    for (int i=0; i<n; ++i) {
        double dnrms = 0;
        for (int j=0; j<n; ++j) dnrms += WW[i][j] * WW[i][j];
        dnrms = sqrt(dnrms);
        dnrms = (dnrms > epsi2 ? sqrt(dnrms) : epsi);
        R[i] = dnrms;
    }
}

/*!
    \brief Truncate the problem to the subspace of interest
    \param x : Current values of the variables (in).
    \param u : Array of highest allowed values (in).
    \param l : Array of lowest allowed values (in).
    \param s : Move in the subspace (in)
    \param arg : List of indices i where s[i] > 0 (in)
    \param narg : size of arg (in).
    \param theta : direction (1 - grad) (in)
    \param rhs : right-hand size of the problem (in)
    \param st : solution of the trust region problem (in)
    \param MM : Cholesky matrix (in).
    \param n : Size of the Cholesky problem (in)
    \param ipt : Indice of the minimum value (out)
    \param alpha : Scaling of the values (out)
    \param mmdis : Minimum value of the displacement (out)
    \return Value corresponding to the best of the three steps.
*/
double truncate(double *x, double *u, double *l, double *s, int *arg, int narg,
    double theta, double *rhs, double st[2], double **MM, int n,
    int *ipt, double *alpha, double *mmdis) {
    *alpha = 1.;
    *mmdis = 1.;
    *ipt = 0;
    if (narg > 0) {
        int j = arg[0];
        double v1 = (u[j]-x[j]) / s[j];
        double v2 = (l[j]-x[j]) / s[j];
        *mmdis = (v1 > v2 ? v1 : v2);
        *ipt = j;
        for (int i=1; i<narg; ++i) {
            j = arg[i];
            v1 = (u[j]-x[j]) / s[j];
            v2 = (l[j]-x[j]) / s[j];
            double dis = (v1 > v2 ? v1 : v2);
            if (dis < *mmdis) {
                *mmdis = dis;
                *ipt = j;
            }
        }
        *alpha = ((*mmdis) * theta > 1.0 ? 1.0 : (*mmdis)*theta);
    }
    double qpval = 0;
    for (int i=0; i<n; ++i) {
        qpval += rhs[i]*st[i];
        for (int j=0; j<n; ++j) qpval += 0.5 * st[i] * MM[i][j] * st[j];
    }
    return qpval * (*alpha);
}

/*!
    \brief Determine the trial step `s', an approx. trust region solution.
    It is chosen as the best of 3 steps: the scaled gradient, a 2-D trust
    region solution and the reflection of the 2-D trust region solution,
    (all are truncated to remain strictly feasible).
    The 2-D subspace (defining the trust region problem) is defined 
    by the scaled gradient direction and a CG process (returning
    either an approximate Newton step of a direction of negative curvature.

    \param x : Array containing the current value of the variables (in).
    \param g : Array containing the current gradient of the cost function (in).
    \param H : Array containing the current Hessian of the cost function (in).
    \param D : Diagonal matrix setting the trust region (in).
    \param delta : Value of the step size (in).
    \param theta : direction (1 - grad) (in).
    \param l : Array of lowest allowed values (in).
    \param u : Array of highest allowed values (in).
    \param flagZ : Indicates if we need to recompute the vectors Z (in).
    \param n : size of x (in).
    \param s : Suggested move from the current x (out; allocated on input).
    \param snod : Moves in the truncated subspace (out; allocated on input).
    \param posdef : Indicates if the Hessian matrix is positive defnite (out).
    \param npcgr : Indicates the number of iteration in pcgr (out).
    \return Size of the move
*/
double trdog(double *x, double *g, double *H, double *D, double delta,
    double theta, double *l, double *u, int flagZ, int n, double *s,
    double *snod, int *npcgr)
{
    const double eps = std::numeric_limits<double>::epsilon();
    static double **Z = NULL;
    static double **WW = NULL;
    static double *grad = NULL;
    static double *DG = NULL;
    static double **MM = NULL;
    static double rhs[2];
    double qp, nqp, alpha, nalpha, mmdis, rhsr;
    double **MMr = new double*[1];
    MMr[0] = new double[1];
    double st[2];
    int ipt, posdef;

    // If the previous step was accepted recompute the arrays
    if (grad == NULL) grad = new double[n];
    if (DG == NULL) DG = new double[n];
    if (Z == NULL) {
        Z = new double*[2];
        Z[0] = new double[2*n];
        Z[1] = &Z[0][n];
    }
    if (WW == NULL) {
        WW = new double*[n];
        WW[0] = new double[n*n];
        for (int i=1; i<n; ++i) WW[i] = WW[0] + i*n;
    }
    if (MM == NULL) {
        MM = new double*[2];
        MM[0] = new double[4];
        MM[1] = &MM[0][2];
    }
    if (flagZ == 0) {
        for (int i=0; i<n; ++i) grad[i] = D[i] * g[i];
        for (int i=0; i<n; ++i) DG[i] = fabs(g[i]);
        for (int i=0; i<n; ++i) {
            int k = i*n;
            for (int j=0; j<i; ++j, ++k) WW[i][j] = D[i]*H[k]*D[j];
            WW[i][i] = D[i]*H[k]*D[i] + DG[i];
            k++;
            for (int j=i+1; j<n; ++j, ++k) WW[i][j] = D[i]*H[k]*D[j];
        }
        double *R = new double[n];
        hprecon(WW, n, R);
        posdef = pcgr(grad, R, WW, n, Z[0], npcgr);
        delete [] R;
        normalize(Z[0], n);
        if (n > 1) {
            if (posdef >= 1) {
                normalize(grad, n, Z[1]);
                orthogonalize(Z[0], Z[1], n);
            } else {
                for (int i=0; i<n; ++i) {
                    Z[1][i] = D[i] * (grad[i] >= 0. ? 1. : -1.);
                    if (grad[i] == 0.) Z[1][i] = 0.;
                }
            }
            normalize(Z[1], n);
        }
        ReduceToSubspace(Z, WW, grad, n, 2, MM, &rhs[0]);
    }

    // Determine 2D TR soln
    trust(&rhs[0], MM, delta, 2, st);
    int *arg = new int[n];
    int narg = 0;
    for (int i=0; i<n; ++i) {
        snod[i] = Z[0][i] * st[0] + Z[1][i] * st[1];
        s[i] = D[i] * snod[i];
        if (fabs(s[i]) > eps) {
            arg[narg] = i;
            narg++;
        }
    }

    // Truncation of the TR solution
    qp = truncate(x,u,l,s,arg,narg,theta,&rhs[0],st,MM,2,&ipt,&alpha,&mmdis);

    if (n > 1) {
        double *ns = new double[n];
        double *nss = new double[n];
        double **ZZ = new double*[1];
        ZZ[0] = new double[n];
        double *ss = new double[n];
        double nrm = 0;
        for (int i=0; i<n; ++i) {
            ss[i] = snod[i] * mmdis;
            nrm += ss[i] * ss[i];
        }
        if (nrm < 0.81*delta*delta) {
            // Evaluate along the reflected direction
            st[0] = mmdis * st[0];
            st[1] = mmdis * st[1];
            double qp0 = rhs[0] * st[0] + rhs[1] * st[1];
            qp0 += 0.5 * st[0] * (MM[0][0] * st[0] + MM[0][1] * st[1]);
            qp0 += 0.5 * st[1] * (MM[1][0] * st[0] + MM[1][1] * st[1]);
            double *r = new double[n];
            for (int i=0; i<n; ++i) r[i] = mmdis * s[i];
            double *ngrad = new double[n];
            for (int i=0; i<n; ++i) {
                nrm = 0;
                int k = i * n;
                for (int j=0; j<n; ++j, ++k) nrm += H[k] * r[j];
                ngrad[i] = D[i] * (g[i] + nrm) + DG[i] * ss[i];
            }
            for (int i=0; i<n; ++i) {
                r[i] += x[i];
                nss[i] = snod[i];
            }
            nss[ipt] *= -1;
            normalize(nss, n, ZZ[0]);
            ReduceToSubspace(ZZ, WW, ngrad, n, 1, MMr, &rhsr);
            delete [] ngrad;
            double tau = quad1d(nss, ss, delta, n);
            nrm = 0;
            narg = 0;
            for (int i=0; i<n; ++i) {
                nrm += nss[i] * nss[i];
                ns[i] = fabs(D[i]) * nss[i];
                if (fabs(ns[i]) > eps) {
                    arg[narg] = i;
                    narg++;
                }
            }
            st[0] = tau / sqrt(nrm);
            nqp = qp0 + truncate(r, u, l, ns, arg, narg, theta, &rhsr, st, MMr, 1,
                &ipt, &nalpha, &mmdis);
            delete [] r;
            if (nqp < qp) {
                for (int i=0; i<n; ++i) {
                    s[i] = ns[i];
                    snod[i] = nss[i]; 
                }
                alpha = nalpha;
                qp = nqp;
            }
        } // End if (nrm < 0.81*delta*delta)
        delete [] ss;

        // Evaluate along the gradient direction
        normalize(grad, n, ZZ[0]);
        ReduceToSubspace(ZZ, WW, grad, n, 1, MMr, &rhsr);
        trust(&rhsr, MMr, delta, 1, st);
        narg = 0;
        for (int i=0; i<n; ++i) {
            nss[i] = ZZ[0][i] * st[0];
            ns[i] = D[i] * nss[i];
            if (fabs(ns[i]) > eps) {
                arg[narg] = i;
                narg++;
            }
        }
        nqp = truncate(x,u,l,ns,arg,narg,theta,&rhsr,st,MMr,1,&ipt,&nalpha,&mmdis);
        if (nqp < qp) {
            for (int i=0; i<n; ++i) {
                s[i] = ns[i];
                snod[i] = nss[i];
            }
            alpha = nalpha;
            qp = nqp;
        }

        delete [] ZZ[0];
        delete [] ZZ;
        delete [] nss;
        delete [] ns;
    } // End if (n > 1)

    if (alpha != 1.) {
        for (int i=0; i<n; ++i) {
            s[i] *= alpha;
            snod[i] *= alpha;
        }
    }

    // Cleaning memory
    delete [] arg;
    delete [] MMr[0];
    delete [] MMr;
    return qp;
}

