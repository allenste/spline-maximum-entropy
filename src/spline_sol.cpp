/*!
    \file spline_sol.cpp
    \author Steve Allen, CCS, Universite de Sherbrooke
    \date September 2011

    \brief This file contains a function to compute the cubic spline
        matrix. It is based on Dominic Bergeron's Matlab program.
*/
/*
 * Copyright (C) 2011 Steve Allen
 *
 * This file is part of SplineMaxEnt.
 * 
 * SplineMaxEnt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SplineMaxEnt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SplineMaxEnt.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "util.h"
#include <cmath>
#include <cstdlib>

/*!
    \brief Function that build the matrix of dependance between the variables
        in the cubic spline formula. Based on Dominic Bergeron's program.
    \param x : list of spline nodes (in).
    \param ns : number of spline nodes (in).
    \param ncut : number of nodes after which the type of spline is changed (in)
    \param xm : Value for the normalisation of x (in).
    \param M : Matrix (out; allocated on input).
*/
void spline_sol(double *x, int nx, int ncut, double xm, double **M) {
    int n = 3 * nx - 1;

    // Build the the spline matrix.
    double **A = new double*[n];
    A[0] = new double[n*n];
    for (int j=0; j<n; ++j) A[0][j] = 0.;
    for (int i=1; i<n; ++i) {
        A[i] = A[i-1] + n;
        for (int j=0; j<n; ++j) A[i][j] = 0.;
    }
    double d = (x[1] - x[0]) / xm;
    A[0][1] = d * d;
    A[0][0] = A[0][1] * d;
    A[1][0] = -6 * d;
    A[1][1] = -2.;
    A[1][3] = 2.;
    A[2][0] = -3 * A[0][1];
    A[2][1] = -2 * d;
    A[2][4] = 1.;
    int k = 3, j = 1;
    for ( ; j<ncut-2; ++j, ++k) {
        d = (x[j+1] - x[j]) / xm;
        A[k][k+1] = d;
        A[k][k] = d * d;
        A[k][k-1] = A[k][k] * d;
        ++k;
        A[k][k-2] = -6 * d;
        A[k][k-1] = -2.;
        A[k][k+2] = 2.;
        ++k;
        A[k][k-3] = -3 * d * d;
        A[k][k-2] = -2 * d;
        A[k][k-1] = -1.;
        A[k][k+2] = 1.;
    }
    d = (x[j+1] - x[j]) / xm;
    A[k][k+1] = d;
    A[k][k] = d * d;
    A[k][k-1] = A[k][k] * d;
    ++k;
    A[k][k-2] = -6 * d;
    A[k][k-1] = -2.;
    double xt = x[j+1] / xm;
    A[k][k+2] = 2. / pow(xt, 4);
    A[k][k+3] = A[k][k+2] * xt;
    ++k;
    A[k][k-3] = -3 * d * d;
    A[k][k-2] = -2 * d;
    A[k][k-1] = -1.;
    A[k][k+2] = -1. / (xt * xt);
    for (++j; j<nx-1; ++j) {
        ++k;
        d = (x[j] - x[j+1]) * xm / (x[j] * x[j+1]);
        A[k][k+1] = d;
        A[k][k] = d * d;
        A[k][k-1] = A[k][k] * d;
        ++k;
        A[k][k-2] = -6 * d;
        A[k][k-1] = -2.;
        A[k][k+2] = 2.;
        ++k;
        A[k][k-3] = -3 * d * d;
        A[k][k-2] = -2 * d;
        A[k][k-1] = -1.;
        A[k][k+2] = 1.;
    }
    ++k;
    d = -1. * xm / x[nx-1];
    A[k][k+1] = d;
    A[k][k] = d * d;
    A[k][k-1] = A[k][k] * d;
    ++k;
    A[k][k-2] = 3 * d * d;
    A[k][k-1] = 2 * d;
    A[k][k] = 1.;

    // Compute the inverse of the spline matrix
    inverseMatrix(A, n);

    // Build the matrix needed for computation
    for (int i=0; i<n; ++i) {
        M[i][0] = -1 * A[i][0];
        k = 0;
        for (j=1; j<nx; ++j) {
            M[i][j] = A[i][k];
            k += 3;
            M[i][j] -= A[i][k];
        }
    }

    // Cleaning the memory
    for (int i=1; i<n; ++i) A[i] = NULL;
    delete [] A[0];
    delete [] A;
}

