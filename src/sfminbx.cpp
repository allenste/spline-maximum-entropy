/*!
    \file sfminbx.cpp
    \author Steve Allen, CCS, Universite de Sherbrooke
    \date August 2011

    \brief This file defines a function that do nonlinear minimization with
    box constraints. It is based on Matlab sfminbx.
*/
/*
 * Copyright (C) 2011 Steve Allen
 *
 * This file is part of SplineMaxEnt.
 * 
 * SplineMaxEnt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SplineMaxEnt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SplineMaxEnt.  If not, see <http://www.gnu.org/licenses/>.
**/
#include "fmincon.h"
#include <cmath>
#include <limits>
#if VERBOSE >= 1
#include <iostream>
#endif

double trdog(double*, double*, double*, double*, double, double, double*,
    double*, int, int, double*, double*, int*);

/*!
    \brief Update the value of the vector v, D, gnrm, theta
    \param x: current value of x
    \param n: number of element of x
    \param g: gradient
    \param l: lowest value
    \param u: highest value
    \param v: difference between x and l or u (direction indicated by g) (out)
    \param D: square root of the absolute value of v (out)
    \param gnrm: largest value of v.*g (out)
    \return theta: 0.95 or 1. - grnm
 */
double update(double *x, int n, double *g, double *l, double *u, double *v,
    double *D, double *gnrm)
{
    double vmax = 0.;
    for (int i=0; i<n; ++i) {
         if (g[i] >= 0.) v[i] = x[i] - l[i];
         else v[i] = x[i] - u[i];
         double buf = fabs(g[i] * v[i]);
         vmax = (vmax >= buf ? vmax : buf);
    }
    for (int i=0; i<n; ++i) D[i] = sqrt(fabs(v[i]));
    *gnrm = vmax;
    return (vmax < 0.5 ? 1. - vmax : 0.95);
}

/*!
    \brief Trust Region Reflective algorithm to solve a minimisation problem
        under constraint (box).
    \param val : Initial value of the cost function (in)
    \param x : Array containing the initial point on input and the best
        solution on output (inout).
    \param g1 : Contains the initial gradient of the cost on input and another
        gradient on output (inout).
    \param H1 : Contains the initial Hessian of the cost on input and another
        hessian on output (inout).
    \param n : Number of variables (in).
    \param l : array of lowest allowed values (in)
    \param u : array of highest allowed values (in).
    \param fct : Pointer to the cost function (in)
    \param arg : Additional argument for the cost function (in).
*/
Results sfminbx(double val, double *x, double *g1, double *H1, int n, double *l,
    double *u, double (*fct)(double*, double*, double*, void*), void *arg)
{
    // Parameters for convegence or stop
    const double tol1 = 1.e-20;
    const double tol2 = 1.e-20;
    const double limit = 0.01 * std::numeric_limits<double>::max();
    const int maxiter = 100;
    const int maxNoAcc = 25;
    int finished, flagZ, npcgr, numAccepChange, noAcc;
    double nrmsx, delta, ratio, gnrm, delbnd, diff, oval;

    // Initialization
    double *xcurr = x;
    double *newx = new double[n];
    double *g = g1;
    double *H = H1;
    double *g2 = new double[n];
    double *H2 = new double[n*n];
    double *newg = g2;
    double *newH = H2;
    double *v = new double[n];
    double *snod = new double[n];
    double *sx = new double[n];
    double *D = new double[n];
    npcgr = 0;
    finished = 0;
    flagZ = 0;
    numAccepChange = 0;
    noAcc = 0;
    nrmsx = 10.;
    delta = 10.;
    ratio = 0.;
    delbnd = 0;
    for (int i=0; i<n; ++i) delbnd += xcurr[i]*xcurr[i];
    delbnd = (delbnd > 0.0001 ? 100 * sqrt(delbnd) : 1.0);
    diff = 100 * val;
    oval = val;
    double theta = update(xcurr, n, g, l, u, v, D, &gnrm);
    if (gnrm < tol1) finished = 3;

    // Main loop
    int iter;
    for (iter=0; iter<maxiter; ++iter) {
        // Determine trust region correction
        double qp = trdog(xcurr,g,H,D,delta,theta,l,u,flagZ,n,sx,snod,&npcgr);

        // Compute x, val, grad, Hessian
        double del = 100 * pow(2., -52);
        for (int i=0; i<n; ++i) {
            newx[i] = xcurr[i] + sx[i];
            if (u[i]-newx[i] < del) newx[i] -= del;
            if (newx[i]-l[i] < del) newx[i] += del;
        }
        double newval = fct(newx, newg, newH, arg);

        // Evaluate the step size
        double aug = 0.;
        nrmsx = 0.;
        for (int i=0; i<n; ++i ) {
            nrmsx += snod[i] * snod[i];
            aug += snod[i] * snod[i] * fabs(newg[i]);
        }
        aug *= 0.5;
        nrmsx = sqrt(nrmsx);
        ratio = (newval + aug - val) / qp;
        if ((ratio >= .75) && (nrmsx >= .9*delta))
            delta = (delbnd < 2*delta ? delbnd : 2*delta);
        else if (ratio<=.25) delta = (nrmsx < delta ? nrmsx*0.25 : delta*0.25);
        if (newval > limit) delta = (nrmsx < delta ? nrmsx*0.05 : delta*0.05);

        // Update
        if (newval < val) {
            oval = val;
            val = newval;
            double *buf = xcurr;
            xcurr = newx;
            newx = buf;
            buf = g;
            g = newg;
            newg = buf;
            buf = H;
            H = newH;
            newH = buf;
            numAccepChange++;
            flagZ = 0;
            noAcc = 0;
            theta = update(xcurr, n, g, l, u, v, D, &gnrm);
            if (gnrm < tol1) finished = 3;
        } else {
            noAcc++;
            flagZ = 1;
        }

        // Test if it is the end
        diff = fabs(oval - val);
        if (nrmsx<.9*delta && ratio>.25 && (diff<tol1*(1.0+fabs(oval))))
            finished = 1;
        else if ((iter > 1) && (nrmsx < tol2)) finished = 2;
        else if (noAcc > maxNoAcc) finished = 6;
        if (finished > 0) break;
    }
    if (iter == maxiter) finished = 5;

    // Copy the result in the vector if not already there
    if (xcurr != x) {
        for (int i=0; i<n; ++i) x[i] = xcurr[i];
        delete [] xcurr;
    } else delete [] newx;

    // Clean the memory
    delete [] H2;
    delete [] g2;
    delete [] v;
    delete [] snod;
    delete [] sx;
    delete [] D;

#if VERBOSE >= 1
    std::cout << "Total number of iterations in pcgr " << npcgr << '\n';
#endif

    // Build the result
    Results r;
    r.mincost = val;
    r.finished = finished;
    r.iter = iter;
    r.numFunEvals = iter;
    r.numAccepChange = numAccepChange;
    r.diff = diff;
    r.gnrm = gnrm;
    r.nrmsx = nrmsx;
    return r;
}
 
