/*!
    \file fmincon.h
    \author Steve Allen, CCS, Universite de Sherbrooke
    \date August 2011

    \brief This file contains a class that enables to build an algorithm for
        the optimisation of a given function under constraint.
*/
/*
 * Copyright (C) 2011 Steve Allen
 *
 * This file is part of SplineMaxEnt.
 * 
 * SplineMaxEnt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SplineMaxEnt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SplineMaxEnt.  If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef FMINCON_H_
#define FMINCON_H_

#include <fstream>
#include <iostream>

/*!
    \brief Structure that enables to stock the statistics of the optimisation
    process in one variable.
*/
typedef struct {
    double mincost;
    int finished;
    int iter;
    int numFunEvals;
    int numAccepChange;
    double diff;
    double gnrm;
    double nrmsx;
} Results;

/*!
    \brief Enumeration of all programmed algorithm.
*/
enum Algorithm {TRUSTREGIONREFLECTIVE};

// List of functions corresponding to the available algorithm
Results sfminbx(double, double*, double*, double*, int, double*, double*,
    double (*fct)(double*, double*, double*, void*), void*);

/*!
    \brief Class to build the optimization algorithm object
*/
class Optimize {
    public:
    Optimize(int, double*, double*);
    Optimize(int, double*, double*, double*, double, double*, double*);

    ~Optimize();

    /*!
        \brief This function prints a desctiption of the algorithm in the
            standard output.
    */
    inline void PrintDescription() const {
        PrintDescription(&std::cout);
    };
    /*!
        \brief This function prints a desctiption of the algorithm in a file.
        \param filename : string that contains the name of the file (in).
    */
    inline void PrintDescription(char *filename) const {
        std::ofstream *out = new std::ofstream(filename);
        PrintDescription(out);
        delete out;
    };
    void PrintDescription(std::ostream *) const;

    /*!
        \brief This function prints the statistics of the optimisation process
            in the standard output.
    */
    inline void PrintStatistics() const {
        PrintStatistics(&std::cout);
    };
    /*!
        \brief This function prints the statistics of the optimisation process
            in a file.
        \param filename : string that contains the name of the file (in).
    */
    inline void PrintStatistics(char *filename) const {
        std::ofstream *out = new std::ofstream(filename);
        PrintStatistics(out);
        delete out;
    };
    void PrintStatistics(std::ostream *) const;

    void SetInitialPoint(double*, double, double*, double*);

    void Driver(double (*fct)(double*,double*,double*,void*), void*);

    /*!
        \brief This function returns a pointer to the gradient
        \return double*
    */
    inline double* getGradPtr() { return grad; }

    /*!
        \brief This function returns a pointer to the Hessian.
        \return double*
    */
    inline double* getHessianPtr() { return H; }

    /*!
        \brief This function sets the value of the cost.
        \param cost : value to put in memory
    */
    inline void setCost(double cost) { result.mincost = cost; }

    /*!
        \brief This function returns the number of accepted moves
        \return Integer corresponding to the number of accepted moves.
    */
    inline int getNumMoves() { return result.numAccepChange; }

    private:
        void SetDefaultValues();

        /*! Enum variable corresponding to the chosen algorithm */
        enum Algorithm algo;

        /*! Structure that stocks the statistics of the optimisation process */
        Results result;

        /*! Number of variables in the optimisation process */
        int nx;

        /*! Pointer to the current values of the variables */
        double *x;

        /*! Pointer to the current gradient of the function we want to minimise */
        double *grad;

        /*! Pointer to the current Hessian of the function we want to minimise */
        double *H;

        /*! Pointer to the lowest value allowed for each variable */
        double *l;

        /*! Pointer to the highest value allowed for each variable */
        double *u;
};

#endif

