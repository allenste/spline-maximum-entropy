/*!
    \file blas.h
    \author Steve Allen, CCS, Universite de Sherbrooke
    \date September 2011

    \brief This file declares Blas and Lapack functions used.
*/
/*
 * Copyright (C) 2011 Steve Allen
 *
 * This file is part of SplineMaxEnt.
 * 
 * SplineMaxEnt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SplineMaxEnt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SplineMaxEnt.  If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef BLAS_H_
#define BLAS_H_

extern "C" {

//    //   Blas level 1 functions  //   //

// Euclidean norm of a double precision vector
double dnrm2_(int*, double*, int*);

// Scalar product between two double precision vector
double ddot_(int*, double*, int*, double*, int*);

// Copy of a double precision vector into another
void dcopy_(int*, double*, int*, double*, int*);

// Scaling of a double precision vector by a constant
void dscal_(int*, double*, double*, int*);

// Add a double precision vector times a constant to another vector
void daxpy_(int*, double*, double*, int*, double*, int*);

//    //   Blas level 2 functions  //   //

// Multiplication of a general matrix by a vector
void dgemv_(char*, int*, int*, double*, double*, int*, double*, int*, double*,
    double*, int*);

// Multiplication of a band matrix by a vector
void dgbmv_(char*, int*, int*, int*, int*, double*, double*, int*, double*,
    int*, double*, double*, int*);

//    //   Blas level 3 functions  //   //

// Multiplication of two double precision matrices
void dgemm_(char*, char*, int*, int*, int*, double*, double*, int*, double*,
    int*, double*, double*, int*);

//    //   Lapack functions  //   //

// Solve a double precision symmetric eigenproblem
void dsyevd_(char*, char*, int*, double*, int*, double*, double*, int*, int*,
    int*, int*);

// Perform an LU decomposition of a matrix
void dgetrf_(int*, int*, double*, int*, int*, int*);

// Usign its LU decomposition inverses a matrix
void dgetri_(int*, double*, int*, int*, double*, int*, int*);

}

#endif

