/*!
    \file kernelSpline.cpp
    \author Steve Allen, CCS, Universite de Sherbrooke
    \date September 2011

    \brief This file contains functions that uses cubic splines to build the
        kernel. It is an adaptation of an algorithm by Dominic Bergeron.
*/
/*
 * Copyright (C) 2011 Steve Allen
 *
 * This file is part of SplineMaxEnt.
 * 
 * SplineMaxEnt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SplineMaxEnt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SplineMaxEnt.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <cmath>
#include <complex>

void spline_sol(double*, int, int, double, double**);

void AddLFContr(double, double, double, double, double*, double**, int, int,
    double*, double);

void AddHFContr(double, double, double, double, double*, double**, int, int,
    int, double*, double);

void computeKernelF(double *kn, int nkn, double *w, int nw, int nw_spline,
    bool sym, std::complex<double> **ker)
{
    // A coder
    return;
}

void computeKernelF(double *kn, int nkn, double *w, int nw, int nw_spline,
    bool sym, double **ker)
{
    // A coder
    return;
}

void computeKernelB(double *kn, int nkn, double *w, int nw, int nw_spline,
    bool sym, std::complex<double> **ker)
{
    // Not sure this can be a possible case.
    return;
}

/*!
    \brief This function computes the kernel for a bosonic real Matsubara
        Green's function. It is based on Dominic Bergeron's work.
    \param kn : Array of Matsubara Frequencies (in).
    \param nkn : Number of Matsubara frequencies (in).
    \param w : Array of real frequencies (in).
    \param nw : Number of real frequencies (in).
    \param nw_cut : Number of frequencies after which we change the type of
        spline (in)
    \param sym : Is the spectrum symmetric around 0 (in).
    \param ker : Array containing the computed kernel (out; allocate on input).
*/
void computeKernelB(double *kn, int nkn, double *w, int nw,  int nw_cut,
    bool sym, double **ker)
{
    int nrow = 3 * nw - 1;
    int nw_lf = nw_cut - 1;
    double wcut = w[nw_lf];

    // Build the spline matrix
    double **Pv = new double*[nrow];
    Pv[0] = new double[nw*nrow];
    for (int i=1; i<nrow; ++i) Pv[i] = Pv[i-1] + nw;
    spline_sol(w, nw, nw_cut, wcut, Pv);

    // Normalisation constant
    double cst = 2.0 / 3.1415926535897932;

    // Building vectors to accelerate computation and make it precise
    double **dis = new double*[nw_lf];
    dis[0] = new double[20*nw_lf];
    for (int i=1; i<nw_lf; ++i) dis[i] = dis[i-1] + 20;
    double wc = w[0];
    for (int i=0; i<nw_lf; ++i) {
        double wp = wc;
        wc = w[i+1];
        double r = wp / wc;
        double r2 = r * r;
        double buf = r;
        // dis[i][j]=(1-(w(i)/w(i+1))^(2j+1))/(2j+1)
        for (int j=0; j<10; ++j) {
            dis[i][j] = (1. - buf) / (2*j + 1) * cst;
            buf *= r2;
        }
        buf= r;
        // dis[i][j+10]=(w(i+1)^(2j)-w(i)^(2j))/(2j)
        for (int j=10; j<20; ++j) {
            // (w(i+1)^(2n)-w(i)^(2n))/(2n)=0.5*(w(i+1)^n+w(i)^n)*(w(i+1)^n-w(i)^n)/n
            dis[i][j] = dis[i][(j-10)/2] * (1.0 + buf) * 0.5;
            buf *= r;
            ++j;
            dis[i][j] = dis[i][(j-11)/2+10] * (1.0 + buf) * 0.5;
            buf *= r;
        }
    }
    int nw_hf = nw - nw_cut + 1;
    double *whf = &w[nw_lf];
    double *x = new double[nw_hf+1];
    for (int i=0; i<nw_hf; ++i) x[i] = 1. / whf[i];
    x[nw_hf] = 0.;
    double **dhf = new double*[nw_hf];
    dhf[0] = new double[19*nw_hf];
    for (int i=1; i<nw_hf; ++i) dhf[i] = dhf[i-1] + 19;
    double uc = x[0];
    for (int i=0; i<nw_hf-1; ++i) {
        double up = uc;
        uc = x[i+1];
        double r = uc / up;
        dhf[i][0] = -1 * log(r) * cst;
        double buf = r;
        for (int j=1; j<18; ++j) {
            dhf[i][j] = (1.0 - buf) / j * cst;
            buf *= r;
        }
        dhf[i][18] = (whf[i+1] - whf[i]) * cst;
    }
    dhf[nw_hf-1][0] = 0.;
    for (int j=1; j<18; ++j) dhf[nw_hf-1][j] = cst / j;
    dhf[nw_hf-1][18] = 0.;

    // Computing the kernel
    // Compute the case qn = 0
    for (int i=0; i<nw; ++i) ker[0][i] = 0;
    // Low frequency contribution
    for (int i=0; i<nw_lf; ++i) {
        double val = w[i+1];
        double kd = dis[i][0] * val;
        val *= val;
        double kc = dis[i][10] * val;
        double kb = dis[i][1] * val * w[i+1];
        val *= val;
        double ka = dis[i][11] * val;
        AddLFContr(ka, kb, kc, kd, ker[0], Pv, i, nw, w, wcut);
    }

    // High frequency contribution
    for (int i=0; i<nw_hf; ++i) {
        double val = x[i];
        double ka = dhf[i][2] * val * val;
        double kb = dhf[i][1] * val;
        double kc = dhf[i][0];
        double kd = dhf[i][18];
        AddHFContr(ka, kb, kc, kd, ker[0], Pv, nw, i, nw_hf, x, wcut);
    }

    // Other values of qn
    #ifdef _OPENMP
    #pragma omp parallel for
    #endif
    for (int i=1; i<nkn; ++i) {
        for (int j=0; j<nw; ++j) ker[i][j] = 0.;
        double kni = kn[i];
        double kni2 = kni * kni;
        // Computing the low frequency contribution to the kernel
        double limit1 = kni * 0.1;
        double limit2 = kni / 6.0;
        int j = 0;
        double wj = w[1];
        double wj2 = w[0]*w[0];
        while (wj < limit1 && j < nw_lf) {
            wj2 = wj * wj;
            double val = wj2 / kni2;
            double buf = pow(val, 8);
            double ka = (dis[j][12] - (dis[j][13] - (dis[j][14] - (dis[j][15] -
                (dis[j][16] - (dis[j][17] - (dis[j][18] - dis[j][19]*val)*val)
                *val)*val)*val)*val)*val)*val*wj2*wj2;
            double kc = (dis[j][11] - dis[j][19]*buf)*val*wj2 - ka/kni2;
            double kb = (dis[j][2] - (dis[j][3] - (dis[j][4] - (dis[j][5] -
                (dis[j][6] - (dis[j][7] - (dis[j][8] - dis[j][9]*val)*val)*val)
                *val)*val)*val)*val)*val*wj2*wj;
            double kd = (dis[j][1] - dis[j][9]*buf)*val*wj - kb/kni2;
            AddLFContr(ka, kb, kc, kd, ker[i], Pv, j, nw, w, wcut);
            ++j;
            wj = w[j+1];
        }
        double atanwkn = atan2(w[j], kni);
        while (wj < limit2 && j < nw_lf) {
            wj2 = wj * wj;
            double val = wj2 / kni2;
            double ka = (dis[j][12] - (dis[j][13] - (dis[j][14] - (dis[j][15] -
                (dis[j][16] - (dis[j][17] - (dis[j][18] - dis[j][19]*val)*val)
                *val)*val)*val)*val)*val)*val*wj2*wj2;
            double kc = (dis[j][11] - dis[j][19]*pow(val,8))*val*wj2 - ka/kni2;
            double atanwknp = atanwkn;
            atanwkn = atan2(wj, kni);
            double kd = dis[j][0] * wj - (atanwkn - atanwknp) * kni * cst;
            double kb = dis[j][1] * wj * wj2 - kd * kni2;
            AddLFContr(ka, kb, kc, kd, ker[i], Pv, j, nw, w, wcut);
            ++j;
            wj = w[j+1];
        }
        for ( ; j < nw_lf; ) {
            double wj2p = wj2;
            wj2 = wj * wj;
            double logwkn = log((wj2 + kni2)/(wj2p + kni2));
            double atanwknp = atanwkn;
            atanwkn = atan2(wj, kni);
            double kd = dis[j][0] * wj - (atanwkn - atanwknp) * kni * cst;
            double kb = dis[j][1] * wj * wj2 - kd * kni2;
            double kc = dis[j][10] * wj2 - logwkn * kni2 * 0.5 * cst;
            double ka = dis[j][11] * wj2 * wj2 - kc * kni2;
            AddLFContr(ka, kb, kc, kd, ker[i], Pv, j, nw, w, wcut);
            ++j;
            wj = w[j+1];
        }
        
        // Computing the high frequency contribution to the kernel
        limit1 = 6.0 / kni;
        j = 0;
        wj = x[j];
        wj2 = wj * wj;
        while (wj >= limit1 && j < nw_hf-1) {
            wj = x[j+1];
            wj2 = wj * wj;
            double val = 1.0 / (kni2 * wj2);
            double ka = (dhf[j][0] - (dhf[j][2] - (dhf[j][4] - (dhf[j][6] -
                (dhf[j][8] - (dhf[j][10] - (dhf[j][12] - dhf[j][14]*val)*val)
                *val)*val)*val)*val)*val)/kni2;
            double kb = (dhf[j][1] - (dhf[j][3] - (dhf[j][5] - (dhf[j][7] -
                (dhf[j][9] - (dhf[j][11] - (dhf[j][13] - dhf[j][15]*val)*val)
                *val)*val)*val)*val)*val)*val*wj;
            double kc = (dhf[j][2] - (dhf[j][4] - (dhf[j][6] - (dhf[j][8] -
                (dhf[j][10] - (dhf[j][12] - (dhf[j][14] - dhf[j][16]*val)*val)
                *val)*val)*val)*val)*val)*val;
            double kd = (dhf[j][3] - (dhf[j][5] - (dhf[j][7] - (dhf[j][9] -
                (dhf[j][11] - (dhf[j][13] - (dhf[j][15] - dhf[j][17]*val)*val)
                *val)*val)*val)*val)*val)*val/wj;
            AddHFContr(ka, kb, kc, kd, ker[i], Pv, nw, j, nw_hf, x, wcut);
            ++j;
        }
        atanwkn = atan(wj * kni);
        for ( ; j < nw_hf; ++j) {
            double wj2p = wj2;
            wj = x[j+1];
            wj2 = wj * wj;
            double logwkn = log((1. + kni2 * wj2p) / (1. + kni2 * wj2)) * cst;
            double atanwknp = atanwkn;
            atanwkn = atan(wj * kni);
            double ka = logwkn * 0.5 / kni2;
            double kb = (atanwknp - atanwkn) / kni * cst;
            double kc = dhf[j][0] - 0.5 * logwkn;
            double kd = dhf[j][18] - kb * kni2;
            AddHFContr(ka, kb, kc, kd, ker[i], Pv, nw, j, nw_hf, x, wcut);
        }
    }

    // Cleaning memory
    for (int i=1; i<nw_hf; ++i) dhf[i] = NULL;
    delete [] dhf[0];
    delete [] dhf;
    delete [] x;
    for (int i=1; i<nw_lf; ++i) dis[i] = NULL;
    delete [] dis[0];
    delete [] dis;
    for (int i=1; i<nrow; ++i) Pv[i] = NULL;
    delete [] Pv[0];
    delete [] Pv;
}

/*!
    \brief This function adds contributions from the low frequencies to the
        bosonic kernel.
    \param ka : contribution from the cubic terms (in).
    \param kb : contribution from the square terms (in).
    \param kc : contribution from the linear terms (in).
    \param kd : contribution from the constant terms (in).
    \param ker : pointer to a row of the kernel where we will add the
        contributions (inout)cyberpresse
    \param Pv : Matrix establishing the dependance between the spline variables (in)
    \param j : Frequency index of the contribution (in)
    \param nw : Total number of frequencies (in)
    \param w : Array of frequencies (in)
    \param wm : value of the frequency where the type of spline change (in).
*/
void AddLFContr(double ka, double kb, double kc, double kd, double *ker,
    double **Pv, int j, int nw, double *w, double wm)
{
    double wj = w[j];
    double val1 = (ka - wj*(3*kb - wj*(3*kc - wj*kd))) / (wm*wm*wm);
    double val2 = (kb - wj*(2*kc - wj*kd)) / (wm*wm);
    if (j > 0) {
        double val3 = (kc - wj * kd) / wm;
        double *pv1 = Pv[3 * j - 1];
        double *pv2 = Pv[3 * j];
        double *pv3 = Pv[3 * j + 1];
        for (int i=0; i<nw; ++i) {
            ker[i] += val1 * pv1[i] + val2 * pv2[i] + val3 * pv3[i];
        }
    } else {
        double *pv1 = Pv[0];
        double *pv2 = Pv[1];
        for (int i=0; i<nw; ++i) {
            ker[i] += val1 * pv1[i] + val2 * pv2[i];
        }
    }
    ker[j] += kd;
}

/*!
    \brief This function adds contributions from the low frequencies to the
        bosonic kernel.
    \param ka : contribution from the cubic terms (in).
    \param kb : contribution from the square terms (in).
    \param kc : contribution from the linear terms (in).
    \param kd : contribution from the constant terms (in).
    \param ker : pointer to a row of the kernel where we will add the
        contributions (inout)
    \param Pv : Matrix establishing the dependance between the spline variables (in)
    \param nw : Total number of frequencies (in)
    \param j : Frequency index of the contribution (in)
    \param nx : Number of frequencies where the spline is of the type "1/w" (in).
    \param w : Array of frequencies (in)
    \param wm : value of the frequency where the type of spline change (in).
*/
void AddHFContr(double ka, double kb, double kc, double kd, double *ker,
    double **Pv, int nw, int j, int nx, double *w, double wm)
{
    double wj = w[j];
    if (j < nx - 1) {
        double val1 = (ka - wj*(3*kb - wj*(3*kc - wj*kd))) * (wm*wm*wm);
        double val2 = (kb - wj*(2*kc - wj*kd)) * (wm*wm);
        double val3 = (kc - wj * kd) * wm;
        double *pv1 = Pv[3 * (j + nw - nx) - 1];
        double *pv2 = Pv[3 * (j + nw - nx)];
        double *pv3 = Pv[3 * (j + nw - nx) + 1];
        for (int i=0; i<nw; ++i) {
            ker[i] += val1 * pv1[i] + val2 * pv2[i] + val3 * pv3[i];
        }
        ker[j+nw-nx] += kd;
    } else {
        double val1 = (ka - wj*3*kb) * (wm*wm*wm);
        double val2 = kb * (wm*wm);
        double *pv1 = Pv[3 * (j + nw - nx) - 1];
        double *pv2 = Pv[3 * (j + nw - nx)];
        for (int i=0; i<nw; ++i) {
            ker[i] += val1 * pv1[i] + val2 * pv2[i];
        }
    }
}

