/*!
    \file trust.cpp
    \author Steve Allen, CCS, Universite de Sherbrooke
    \date August 2011

    \brief This file defines a function to compute the exact soln of trust
    region problem. Based on Matlab Trust function.
*/
/*
 * Copyright (C) 2011 Steve Allen
 *
 * This file is part of SplineMaxEnt.
 * 
 * SplineMaxEnt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SplineMaxEnt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SplineMaxEnt.  If not, see <http://www.gnu.org/licenses/>.
**/
#include "util.h"
#include <limits>
#include <cstdlib>
#include <cmath>

/*!
    \brief returns the value of the secular equation at a point lambda :
        1/delta - 1/(||s(lambda)||) = 0
    \param lambda : point where to evaluate the secular equation
    \param eigval : list of eigenvalues
    \param alpha : vector given by V * g where V is the eigenvector matrix
        and g the gradient
    \param delta : step
    \param n : number of eigenvalues
    \return the value of the secular equation.
*/
double seceqn(double lambda, double *eigval, double *alpha, double delta, int n)
{
    const double limit = sqrt(std::numeric_limits<double>::max());
    const double eps = std::numeric_limits<double>::epsilon();
    double value = 0.;
    for (int i=0; i<n; ++i) {
        double v = eigval[i] + lambda;
        if (fabs(v) > eps) v = alpha[i] / v;
        else v = limit;
        value += v * v;
    }
    if (value > eps) value = 1.0 / sqrt(value);
    else value = 0.;
    return 1./delta - value;
}

/*!
    \brief Zero of the function seceqn to the RIGHT of the
        starting point x. A small modification of the M-file fzero,
        described below, to ensure a zero to the Right of x is
        searched for.
    \param x: position where to start the search
    \param eigenval : Second argument of seceqn
    \param alpha : third argument of seceqn
    \param delta : fourth argument of seceqn
    \param n : fifth argumetn of seceqn
    \retrun a double corresponding to the zero of seceqn
*/
double rfzero(double x, double *eigval, double *alpha, double delta, int n) {
    // Initialisation
    const double tol = 1.e-12;
    const int itbnd = 50;
    double dx, fa, fb, fc, a, b, c, d, e, m, p, q, r, toler;
    int itfun;
    if (x != 0.) dx = fabs(x) * 0.5;
    else dx = 0.5;
    a = x;
    fa = seceqn(a, eigval, alpha, delta, n);
    b = x + 1;
    fb = seceqn(b, eigval, alpha, delta, n);
    if ((fa > 0.) != (fb > 0.)) dx *= 2;
    c = a;
    itfun = 2;
    fc = fb;

    // Main loop
    while (fb != 0.) {
        // Insure that b is the best result so far, a is the previous value of
        // b and c is on the opposite of the zero from b.
        if ((fa > 0.) == (fc > 0.)) {
            c = a;
            fc = fa;
            d = b - a;
            e = d;
        }
        if (fabs(fc) < fabs(fb)) {
            a = b;
            b = c;
            c = a;
            fa = fb;
            fb = fc;
            fc = fa;
        }

        // Convergence test
        if (itfun > itbnd) break;
        m = 0.5 * (c - b);
        toler = 2 * tol * (fabs(b) > 1. ? fabs(b) : 1);
        if ((fabs(m) <= toler) || (fb == 0.)) break;

        // Choose bisection or interpolation
        if ((fabs(e) < toler) || (fabs(a) <= fabs(fb))) {
            // bisection
            d = m;
            e = m;
        } else {
            // interpolation
            double s = fb/fa;
            if (a == c) {
                // Linear interpolation
                p = 2 * m * s;
                q = 1.0 - s;
            } else {
                // Inverse quadratic interpolation
                q = fa / fc;
                r = fb / fc;
                p = s * (2 * m * q * (q - r) - (b - a) * (r - 1.0));
                q = (q - 1.0) * (r - 1.0) * (s - 1.0);
            }
            if (p > 0.) q *= -1;
            else p *= -1;
            // Is interpolated point acceptable
            if ((2*p < 3*m*q - fabs(toler*q)) && (p < fabs(0.5*e*q))) {
                e = d;
                d = p / q;
            } else {
                d = m;
                e = m;
            }
        } // End interpolation

        // Next point
        a = b;
        fa = fb;
        if (fabs(d) > toler) b += d;
        else if (b > c) b -= toler;
        else b += toler;
        fb = seceqn(b, eigval, alpha, delta, n);
        itfun++;
    } // End while loop
    return b;
}

/*!
    \brief Solves the trust region problem:
        min{g^Ts + 1/2 s^THs: ||s|| <= delta}. The full eigen-decomposition is
        used; based on the secular equation, 1/delta - 1/(||s||) = 0.
    \param g : gradient vector
    \param H : Matrix Z*(D*HH*D + DG)*Z where HH is the hessian (see trdog)
    \param delta : step
    \parma n : Number of elements in g
    \param s : solution of the trust region problem.
*/
void trust(double *g, double **H, double delta, int n, double *s) {
    // Initialization
    const double tol = 1.e-8;
    const double limit = std::numeric_limits<double>::max();
    const double eps = 10 * std::numeric_limits<double>::epsilon();
    int key = 0;
    double laminit, **V, D[n], coeff[n], alpha[n];
    V = new double*[n];
    V[0] = new double[n*n];
    for (int i=1; i<n; ++i) V[i] = V[i-1] + n;
    eigen(H[0], n, V[0], D);
    for (int i=0; i<n; ++i) {
        double sum = 0;
        for (int j=0; j<n; ++j) sum -= V[j][i] * g[j];
        alpha[i] = sum;
    }

    // positive definite case
    if (D[0] > 0.) {
        for (int i=0; i<n; ++i) coeff[i] = alpha[i] / D[i];
        double nrms = 0;
        for (int i=0; i<n; ++i) {
            double sum = 0;
            for (int j=0; j<n; ++j) sum += V[i][j] * coeff[j];
            nrms += sum * sum;
            s[i] = sum;
        }
        if (nrms < 1.44 * delta * delta) key = 1;
        else laminit = 0.;
    } else {
        for (int i=0; i<n; ++i) coeff[i] = 0;
        laminit = -1 * D[0];
    }

    // Indefinite case
    if (key == 0) {
        int sig = (alpha[0] >= 0. ? 1 : -1);
        double lambda = -1 * D[0];
        if (seceqn(laminit, D, alpha, delta, n) > 0.) {
            lambda = rfzero(laminit, D, alpha, delta, n);
            if (fabs(seceqn(lambda, D, alpha, delta, n)) <= tol) {
                key = 2;
                for (int i=0; i<n; ++i) {
                    double w = lambda + D[i];
                    if (w != 0.) coeff[i] = alpha[i] / w;
                    else if (alpha[i] == 0.) coeff[i] = 0.;
                    else coeff[i] = limit;
                }
                double nrms = 0;
                for (int i=0; i<n; ++i) {
                    double sum = 0;
                    for (int j=0; j<n; ++j) sum += V[i][j] * coeff[j];
                    nrms += sum * sum;
                    s[i] = sum;
                }
                if (nrms > 1.44*delta*delta || nrms < 0.64*delta*delta) {
                    key = 5;
                    lambda = -1 * D[0];
                }
            } // End if (fabs(seceqn(lambda,D,alpha,delta,n)) <= tol2)
            else {
                key = 3;
                lambda = -1 * D[0];
            }
        } // End if (seceqn(laminit, D, alpha, delta, n) > 0.)
        else key = 4;
        if (key > 2) {
            for (int i=0; i<n; ++i) {
                double val = (fabs(D[i]) > 1. ? fabs(D[i]) : 1.);
                if (fabs(lambda + D[i]) < eps * val) alpha[i] = 0.;
            }
        }
        for (int i=0; i<n; ++i) {
            double w = lambda + D[i];
            if (w != 0.) coeff[i] = alpha[i] / w;
            else if (alpha[i] == 0.) coeff[i] = 0.;
            else coeff[i] = limit;
        }
        double nrms = 0;
        for (int i=0; i<n; ++i) {
            double sum = 0;
            for (int j=0; j<n; ++j) sum += V[i][j] * coeff[j];
            nrms += sum * sum;
            s[i] = sum;
        }
        if ((key > 2) && (nrms < 0.64 * delta * delta)) {
            double beta = sqrt(delta * delta - nrms * nrms);
            for (int i=0; i<n; ++i) s[i] += beta * sig * V[0][i];
        } else if ((key > 2) && (nrms > 1.44 * delta * delta)) {
            lambda = rfzero(laminit, D, alpha, delta, n);
            for (int i=0; i<n; ++i) {
                double w = lambda + D[i];
                if (w != 0.) coeff[i] = alpha[i] / w;
                else if (alpha[i] == 0.) coeff[i] = 0.;
                else coeff[i] = limit;
            }
            for (int i=0; i<n; ++i) {
                double sum = 0;
                for (int j=0; j<n; ++j) sum += V[i][j] * coeff[j];
                s[i] = sum;
            }
        } // End else if ((key>2) && (nrms>1.44*delta*delta))
    } // End if (key == 0)
    for (int i=1; i<n; ++i) V[i] = NULL;
    delete [] V[0];
    delete [] V;
}

